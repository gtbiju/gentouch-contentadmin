package com.cognizant.le.model.dao;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import com.cognizant.le.model.dto.Carrier;
import com.cognizant.le.model.dto.CodeLocaleLookup;
import com.cognizant.le.model.dto.CodeLookup;
import com.cognizant.le.model.dto.CodeRelation;
import com.cognizant.le.model.dto.CodeTypeLookup;

// TODO: Auto-generated Javadoc
/**
 * The Interface LookupDAO.
 */
public interface LookupDAO {

	/**
	 * Insert carriers.
	 *
	 * @param carrierList the carrier list
	 */
	public void insertCarriers(List<Carrier> carrierList);

	/**
	 * Insert types.
	 *
	 * @param codeTypes the code types
	 */
	public void insertTypes(List<CodeTypeLookup> codeTypes);

	/**
	 * Insert look up.
	 *
	 * @param codeLookups the code lookups
	 */
	public void insertLookUp(List<CodeLookup> codeLookups);

	/**
	 * Insert relations.
	 *
	 * @param codeRelations the code relations
	 */
	public void insertRelations(List<CodeRelation> codeRelations);

	/**
	 * Insert locale.
	 *
	 * @param codeLocaleLookups the code locale lookups
	 */
	public void insertLocale(List<CodeLocaleLookup> codeLocaleLookups);
	
	/**
	 * Delete existing entries.
	 */
	public void deleteExistingEntries();
	
	/**
	 * Check table.
	 *
	 * @param schemaName the schema name
	 * @param tableName the table name
	 * @return the boolean
	 */
	public Boolean checkTable(String schemaName, String tableName);
	
	
	/**
	 * Creates the table.
	 *
	 * @param isr the isr
	 */
	public void createTable(InputStreamReader isr);
	
	public void turnOnIdentityInsert(String tableName) throws SQLException;

}
