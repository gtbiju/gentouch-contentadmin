package com.cognizant.le.manager;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.cognizant.le.model.dao.AgentServiceDAO;
import com.cognizant.le.model.dao.AgentServiceDaoImpl;
import com.cognizant.le.model.dto.Achievement;
import com.cognizant.le.model.dto.AchievementDetailsDTO;
import com.cognizant.le.model.dto.AgentAchievmentDTO;
import com.cognizant.le.model.dto.AgentDTO;
import com.cognizant.le.model.dto.AgentProfile;
import com.cognizant.le.util.Constants;
import com.cognizant.le.util.LoggerUtil;

public class AgentServiceManagerImpl implements AgentServiceManager {
	static Logger logger = Logger.getLogger(AgentServiceManagerImpl.class);
	
	@Override
	public boolean isValidUser(String userid,String password ) throws Exception {
		boolean checkUserFlag= false;
		logger.debug(LoggerUtil.getLogMessage("Method Entry : AgentServiceManagerImpl.isValidUser  for Agent:", userid));
		AgentServiceDaoImpl agentServicedao = new AgentServiceDaoImpl();
		try {
			checkUserFlag = agentServicedao.isValidUser(userid, password);

		} catch (Exception e) {
			logger.error(LoggerUtil.getLogMessage("Error while getting isValidUser Class for Agent:", userid),e);
			throw new Exception(e);
		}
		logger.debug(LoggerUtil.getLogMessage("Method : AgentServiceManagerImpl.isValidUsere  Agent:", userid));
		logger.debug(LoggerUtil.getLogMessage("Method : AgentServiceManagerImpl.checkUserFlag  Agent:", checkUserFlag+""));
		logger.debug(LoggerUtil.getLogMessage("Method Exit : AgentServiceManagerImpl.isValidUsere  Agent:", userid));
		
		return checkUserFlag;
	}
	@Override
	public Set<AgentDTO> saveToAgentDB(Set<AgentDTO> agents, List<AgentAchievmentDTO> achievments, Set<AchievementDetailsDTO> achievementDtls, String opMode) throws Exception {
        
		Set<AgentDTO> agentsFromDB= new HashSet<AgentDTO>();
		Set<AgentDTO> agentsNotValid=new HashSet<AgentDTO>();
		Set<AgentDTO> agentstoDB= new HashSet<AgentDTO>();
		logger.debug(LoggerUtil.getLogMessage("Method Entry : AgentServiceManagerImpl.isValidUser  for Agent:", agents.size()+""));
		//logger.debug(LoggerUtil.getLogMessage("Method Entry : AgentServiceManagerImpl.isValidUser  for Agent:");
		AgentServiceDaoImpl agentServicedao = new AgentServiceDaoImpl();
		
		try {
			
			logger.debug(LoggerUtil.getLogMessage("saveToAgentDB : AgentServiceManagerImpl.saveToAgentDB  agentstoDB size:", agentstoDB.size()+""));
			logger.debug(LoggerUtil.getLogMessage("saveToAgentDB : AgentServiceManagerImpl.saveToAgentDB  agentsNotValid size:", agentsNotValid.size()+""));
			
			agentsFromDB = agentServicedao.saveToDatabase(agents, achievments, achievementDtls, opMode);
			
			logger.debug(LoggerUtil.getLogMessage("saveToAgentDB : AgentServiceManagerImpl.saveToAgentDB Final to UI agentsFromDB size:", agentsFromDB.size()+""));
									
		} catch (Exception e) {
			logger.error(LoggerUtil.getLogMessage("Error while getting User Profile in AgentServiceDaoImpl for Agent:", "public exception"),e);
			throw new Exception(e);
		}
		
		return agentsFromDB;
	}
	
	@Override
	public List<AgentAchievmentDTO> saveToAchievmentsDB(List<AgentAchievmentDTO> achievments) throws Exception {
        
		List<AgentAchievmentDTO> achievmentsFromDB= new ArrayList<AgentAchievmentDTO>();
		List<AgentAchievmentDTO> achievmentsNotValid= new ArrayList<AgentAchievmentDTO>();
		List<AgentAchievmentDTO> achievmentstoDB= new ArrayList<AgentAchievmentDTO>();
		logger.debug(LoggerUtil.getLogMessage("Method Entry : AgentServiceManagerImpl.saveToAchievmentsDB  for Agent:", achievments.size()+""));
		
		//logger.debug(LoggerUtil.getLogMessage("Method Entry : AgentServiceManagerImpl.isValidUser  for Agent:");
		AgentServiceDaoImpl agentServicedao = new AgentServiceDaoImpl();
		
		try {
			
			for (AgentAchievmentDTO achievment : achievments) {
			boolean checkAgentExists=false;	
			String agentID=achievment.getAgentCode();
			checkAgentExists = agentServicedao.isValidAgent(agentID);
			if ((checkAgentExists)){ 	
				achievmentstoDB.add(achievment);
			}
			else{
				achievment.setStatus(Constants.ACHIEVMENT_STATUS_INVALID);
				achievment.setMessage(Constants.ACHV_INVALID);
				achievmentsNotValid.add(achievment);
			}
					
			}
			logger.debug(LoggerUtil.getLogMessage("saveToAgentDB : AgentServiceManagerImpl.saveToAgentDB  achievmentstoDB size:", achievmentstoDB.size()+""));
			logger.debug(LoggerUtil.getLogMessage("saveToAgentDB : AgentServiceManagerImpl.saveToAgentDB  achievmentsNotValid size:", achievmentsNotValid.size()+""));
			achievmentsFromDB=agentServicedao.insertAchievments(achievmentstoDB);
			
		    for (AgentAchievmentDTO achievment : achievmentsNotValid) {
				achievmentsFromDB.add(achievment);
				
			} 
			logger.debug(LoggerUtil.getLogMessage("saveToAgentDB : AgentServiceManagerImpl.saveToAgentDB Final to UI achievmentsFromDB size:", achievmentsFromDB.size()+""));
									
		} catch (Exception e) {
			logger.error(LoggerUtil.getLogMessage("Error while getting User Profile in AgentServiceDaoImpl for Achievement:", "public exception"),e);
			throw new Exception(e);
		}
		
		return achievmentsFromDB;
	}
	
  
	public  AgentProfile combineDTO(Set<AgentDTO> agents, List<AgentAchievmentDTO> achievments) throws Exception {
		
		logger.debug(LoggerUtil.getLogMessage("Method Entry : AgentServiceManagerImpl.combineDTO  for Agent:", agents.size()+""));
		//logger.debug(LoggerUtil.getLogMessage("Method Entry : AgentServiceManagerImpl.combineDTO  for Agent:", achievments.size()+""));
		
		List<AgentAchievmentDTO> achievmentsOrphan= new ArrayList<AgentAchievmentDTO>();
		
		List<AgentAchievmentDTO> mergedachievmentsOrphan= new ArrayList<AgentAchievmentDTO>();
		
		Map <String,AgentAchievmentDTO> agentMap = new HashMap <String,AgentAchievmentDTO>();
		
		AgentProfile profile= new AgentProfile();
		
		
		int successAchvCount=0;int failAchvCount=0;int invalidAchvCount=0; int totalAchvCount= null!=achievments? achievments.size(): 0;
		for (AgentAchievmentDTO achievment : achievments) {
			 if ( null!= achievment.getStatus() && achievment.getStatus().equals(Constants.ACHIEVMENT_STATUS_SUCCESS)) {
				 successAchvCount=successAchvCount+1;	
				}else if (null!= achievment.getStatus() && achievment.getStatus().equals(Constants.ACHIEVMENT_STATUS_FAILURE)){
					failAchvCount=failAchvCount+1;
				}else if (null!= achievment.getStatus() && achievment.getStatus().equals(Constants.ACHIEVMENT_STATUS_INVALID)){
					invalidAchvCount=invalidAchvCount+1;
			    }
     
		}
		
		profile.setTotalAchievments(totalAchvCount);
		profile.setSuccessAchievments(successAchvCount);
		profile.setInvalidAchievments(invalidAchvCount);
		profile.setFailAchievments(failAchvCount);
		
		
		try {
			int successAgentCount=0;int failAgentCount=0;int invalidAgentCount=0; int totalAgentCount=agents.size();	
			
		for (AgentDTO agent : agents) {	
			String agentID=agent.getAgentCode();
			String messageForAgent = null!=agent.getMessage()?agent.getMessage()+",":"--";
			String messageForAchievmentStart ="[Achievement Details :";
			String closingBrackets="]";
			String statusForAgent=null!=agent.getMessage()?agent.getStatus():"--";
			String statusForAchievment="";
			String  mAchivement="";
			String combineAch="";
			   if (null!=agent.getStatus() && agent.getStatus().equals(Constants.AGENT_STATUS_SUCCESS)) {
				   successAgentCount=successAgentCount+1;	
				}else if (null!=agent.getStatus() && agent.getStatus().equals(Constants.AGENT_STATUS_FAILURE)){
					failAgentCount=failAgentCount+1;
				}else if (null!=agent.getStatus() &&  agent.getStatus().equals(Constants.AGENT_STATUS_INVALID)){
					invalidAgentCount=invalidAgentCount+1;
			    }
		    int index=0;
			for (AgentAchievmentDTO achievment : achievments) {
			
				if (null!=achievment.getAgentCode()&&(achievment.getAgentCode().equals(agentID))){
					mAchivement =mAchivement+achievment.getMessage()+",";
					achievment.setOrphanChild(true);
					statusForAchievment=statusForAchievment+achievment.getStatus()+" / ";
					index=index+1;
			   }
			}
			combineAch=messageForAgent;
			
			agent.setStatus(statusForAgent);
			agent.setMessage(combineAch);
	
	    }
		
		for (AgentAchievmentDTO achievment : achievments) {
			
			if (!achievment.isOrphanChild()){
				
				achievmentsOrphan.add(achievment);	
			
		   }
			
		 }
		
	 	
	    agentMap= new HashMap<String,AgentAchievmentDTO>();
		for (AgentAchievmentDTO achievment : achievmentsOrphan) {
			String agentCode=achievment.getAgentCode();
			
			
			if (agentMap.containsKey(agentCode)){
				AgentAchievmentDTO dto= agentMap.get(agentCode);
				dto.setAgentCode(achievment.getAgentCode());
				dto.setStatus(dto.getStatus()+ " / "+achievment.getStatus());
				dto.setMessage(dto.getMessage()+","+achievment.getMessage());
				agentMap.put(agentCode, dto);
			
			}else {
			
				agentMap.put(agentCode, achievment);
				
			}
				
		 }
		
		
		
		
		for (Map.Entry entry : agentMap.entrySet()) {
			
			mergedachievmentsOrphan.add(agentMap.get(entry.getKey()));
        }
		
		for (AgentAchievmentDTO achievment : mergedachievmentsOrphan) {
			AgentDTO agentdto = new AgentDTO();
			agentdto.setAgentCode(achievment.getAgentCode());
			agentdto.setStatus(achievment.getStatus());
			agentdto.setMessage(achievment.getMessage());
			agents.add(agentdto);
		}
			
		profile.setTotalAgents(totalAgentCount);
		profile.setSuccessAgents(successAgentCount);
		profile.setInvalidAgent(invalidAgentCount);
		profile.setFailAgents(failAgentCount);
		profile.setAgents(new ArrayList<AgentDTO>(agents));
		
		logger.debug(LoggerUtil.getLogMessage("saveToAgentDB : AgentServiceManagerImpl.saveToAgentDB Final to UI achievments without Agent details size:", achievmentsOrphan.size()+""));
			
		
		}catch (Exception e){
			logger.error(LoggerUtil.getLogMessage("Error while combineDTO in AgentServiceDaoImpl for Achievement/Agents:", "public exception"),e);
			throw new Exception(e);
			
		}
		logger.debug(LoggerUtil.getLogMessage("Method Exit : AgentServiceManagerImpl.combineDTO to UI  for Agent:", agents.size()+""));	
	  return profile;
		
	    }
	
	public AgentProfile saveAgentProfile(AgentProfile agentProfile, String opMode) throws Exception {
		AgentProfile rsAgentProfile=null;
		logger.debug(LoggerUtil.getLogMessage("Method Entry : AgentServiceManagerImpl.saveAgentProfile  for Agent:"));
		Set<AgentDTO> agents = null;
		List<AgentAchievmentDTO> achievments = null;
		Set<AchievementDetailsDTO> achievementDtls = null;
		try {
			if(null!=agentProfile.getAgentsMap() && agentProfile.getAgentsMap().containsKey("VALID")){
				agents = agentProfile.getAgentsMap().get("VALID");
				if(null!=agentProfile.getAchievementsMap() && agentProfile.getAchievementsMap().containsKey("VALID")){
					achievments = agentProfile.getAchievementsMap().get("VALID");
				}
				if(null!=agentProfile.getAchievementDtlsMap() && agentProfile.getAchievementDtlsMap().containsKey("VALID")){
					achievementDtls = agentProfile.getAchievementDtlsMap().get("VALID");
				}
			}
			Set<AgentDTO> agentsFromService=saveToAgentDB(agents ,achievments , achievementDtls, opMode);
			rsAgentProfile=combineDTO(agentsFromService,achievments);
	}catch (Exception e){
		logger.error(LoggerUtil.getLogMessage("Error while combineDTO in AgentServiceDaoImpl for Achievement/Agents:", "public exception"),e);
		throw new Exception(e);
		
	}
	logger.debug(LoggerUtil.getLogMessage("Method Exit : AgentServiceManagerImpl.saveAgentProfile to UI  for Agent:", ""));		
	return 	rsAgentProfile;
		
	}
	
	
	public String getChannelIDForName(String channel){
		AgentServiceDAO agentServiceDAO = new AgentServiceDaoImpl();
		String id = agentServiceDAO.getChannelID(channel);
		return id;
	}
	
	
	

}
