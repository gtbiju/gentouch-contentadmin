package com.cognizant.le.model.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Null;

// TODO: Auto-generated Javadoc
/**
 * The Class ContentFile.
 */
@NamedQueries({
		@NamedQuery(name = "fetchContentFileByFileName", query = "from ContentFile contentFile where fileName = :fileName"),
		@NamedQuery(name = "fetchSortedContentFiles", query = "FROM ContentFile ORDER BY lastUpdated DESC"),
		@NamedQuery(name = "filteredContentFiles", query = "FROM ContentFile WHERE type_id= :filter ORDER BY lastUpdated DESC"),
		@NamedQuery(name = "fetchContentFileByPriority", query = "FROM ContentFile contentFile WHERE fileName IN (:fileNames) ORDER BY contentFile.contentType.priority"),
		@NamedQuery(name = "fetchContentFileByAppContext", query = "select contentFile from ContentFile AS contentFile LEFT JOIN contentFile.appContext AS cappContext where cappContext.id in (:applicationcontextIdList) ORDER BY contentFile.contentType.priority"),
		@NamedQuery(name = "fetchContentFileByAppContextAndType", query = "SELECT contentFile from ContentFile AS contentFile LEFT JOIN contentFile.appContext AS cappContext where cappContext.id in (:applicationcontextIdList) AND contentFile.contentType.type = :type  ORDER BY contentFile.contentType.priority"),
		@NamedQuery(name = "fetchContentFileByAppContextGlobal", query = "SELECT  contentFile from ContentFile AS contentFile LEFT JOIN contentFile.appContext AS cappContext where cappContext.id in (:applicationcontextIdList) AND (contentFile.contentType.type = :type OR :contentTypeOptional = 1) AND (contentFile.contentLanguage.language = :name OR :contentLangOptional = 1) AND (contentFile.isMandatory = :isMandatory OR :isMandatoryOptional = 1) ORDER BY contentFile.contentType.priority") })
@Entity
@Table(name = "CONTENTFILES")
public class ContentFile implements Comparable<ContentFile> {

	/** The file name. */
	@Id
	@Column(name = "fileName")
	private String fileName;

	/** The version. */
	@Column(name = "version")
	private Integer version;

	/** The last updated. */
	@Column(name = "lastUpdated")
	private Date lastUpdated;

	/** The public url. */
	@Column(name = "publicURL")
	private String publicURL;

	/** The tablet full path. */
	@Column(name = "tabletFullPath")
	private String tabletFullPath;

	/** The content type. */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Type_Id")
	private ContentType contentType;

	/** The comments. */
	@Column(name = "comments")
	private String comments;

	/** The app context. */
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "CONTENTFILES_APPCONTEXT_X", joinColumns = @JoinColumn(name = "contentFile"), inverseJoinColumns = @JoinColumn(name = "appContextId"))
	private List<AppContext> appContext;

	/** The content language. */
	@ManyToOne(fetch= FetchType.EAGER)
	@JoinColumn(name = "lang_id", referencedColumnName = "id", nullable=true)
	private ContentLanguage contentLanguage;
	
	/** The is mandatory. */
	@Column(name = "is_mandatory")
	private Boolean isMandatory;
	
	/** The file size in bytes. */
	@Column(name="size")
	private Long fileSize;
	
	/** The description of the file. */
	@Column(name="description")
	private String description;

	/**
	 * Gets the content type.
	 * 
	 * @return the content type
	 */
	public ContentType getContentType() {
		return contentType;
	}

	/**
	 * Sets the type.
	 * 
	 * @param contentType
	 *            the new type
	 */
	public void setType(ContentType contentType) {
		this.contentType = contentType;
	}

	/**
	 * Gets the file name.
	 * 
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 * 
	 * @param fileName
	 *            the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the tablet full path.
	 * 
	 * @return the tablet full path
	 */
	public String getTabletFullPath() {
		return tabletFullPath;
	}

	/**
	 * Sets the tablet full path.
	 * 
	 * @param tabletFullPath
	 *            the new tablet full path
	 */
	public void setTabletFullPath(String tabletFullPath) {
		this.tabletFullPath = tabletFullPath;
	}

	/**
	 * Gets the public url.
	 * 
	 * @return the public url
	 */
	public String getPublicURL() {
		return publicURL;
	}

	/**
	 * Sets the public url.
	 * 
	 * @param publicURL
	 *            the new public url
	 */
	public void setPublicURL(String publicURL) {
		this.publicURL = publicURL;
	}

	/**
	 * Gets the last updated.
	 * 
	 * @return the last updated
	 */
	public Date getLastUpdated() {
		return lastUpdated;
	}

	/**
	 * Sets the last updated.
	 * 
	 * @param lastUpdated
	 *            the new last updated
	 */
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 * 
	 * @param version
	 *            the new version
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	/**
	 * Gets the comments.
	 * 
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 * 
	 * @param comments
	 *            the new comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ContentFile o) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Gets the app context.
	 * 
	 * @return the app context
	 */
	public List<AppContext> getAppContext() {
		return appContext;
	}

	/**
	 * Sets the app context.
	 * 
	 * @param appContext
	 *            the new app context
	 */
	public void setAppContext(List<AppContext> appContext) {
		this.appContext = appContext;
	}

	/**
	 * Gets the content language.
	 * 
	 * @return the content language
	 */
	public ContentLanguage getContentLanguage() {
		return contentLanguage;
	}

	/**
	 * Sets the content language.
	 * 
	 * @param contentLanguage
	 *            the new content language
	 */
	public void setContentLanguage(ContentLanguage contentLanguage) {
		this.contentLanguage = contentLanguage;
	}

	/**
	 * Gets the checks if is required.
	 *
	 * @return the checks if is required
	 */
	public Boolean getIsRequired() {
		return isMandatory;
	}

	/**
	 * Sets the checks if is required.
	 *
	 * @param isRequired the new checks if is required
	 */
	public void setIsRequired(Boolean isRequired) {
		this.isMandatory = isRequired;
	}

	/**
	 * Instantiates a new content file.
	 *
	 * @param fileName the file name
	 * @param version the version
	 * @param lastUpdated the last updated
	 * @param publicURL the public url
	 * @param tabletFullPath the tablet full path
	 * @param contentType the content type
	 * @param comments the comments
	 * @param appContext the app context
	 * @param contentLanguage the content language
	 * @param isMandatory the is mandatory
	 */
	public ContentFile(String fileName, Integer version, Date lastUpdated,
			String publicURL, String tabletFullPath, ContentType contentType,
			String comments, List<AppContext> appContext,
			ContentLanguage contentLanguage, Boolean isMandatory) {
		super();
		this.fileName = fileName;
		this.version = version;
		this.lastUpdated = lastUpdated;
		this.publicURL = publicURL;
		this.tabletFullPath = tabletFullPath;
		this.contentType = contentType;
		this.comments = comments;
		this.appContext = appContext;
		this.contentLanguage = contentLanguage;
		this.isMandatory = isMandatory;
	}

	/**
	 * Instantiates a new content file.
	 */
	public ContentFile(){
		super();
	}

	/**
	 * Gets the checks if is mandatory.
	 *
	 * @return the checks if is mandatory
	 */
	public Boolean getIsMandatory() {
		return isMandatory;
	}

	/**
	 * Sets the checks if is mandatory.
	 *
	 * @param isMandatory the new checks if is mandatory
	 */
	public void setIsMandatory(Boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	/**
	 * Gets the file size.
	 *
	 * @return the file size
	 */
	public Long getFileSize() {
		return fileSize;
	}

	/**
	 * Sets the file size.
	 *
	 * @param fileSize the new file size
	 */
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the version.
	 *
	 * @param version the new version
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}

	/**
	 * Sets the content type.
	 *
	 * @param contentType the new content type
	 */
	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}
}
