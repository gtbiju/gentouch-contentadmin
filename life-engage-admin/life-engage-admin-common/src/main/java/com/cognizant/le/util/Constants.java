package com.cognizant.le.util;

// TODO: Auto-generated Javadoc
/**
 * The Class Constants.
 */
public final class Constants {
	
	/** The Constant HIBERNATE_PROPERTY. */
	public static final String HIBERNATE_PROPERTY = "//webapps//resources//hibernate.properties";
	
	/** The Constant SERVER_BASE. */
	public static final String SERVER_BASE = System
			.getProperty("catalina.base");
	
	/** The Constant VERSION. */
	public static final String VERSION = "version";	
	
	/** The Constant AGENT_PROFILE_PATH. */
	public static final String AGENT_PROFILE_PATH = "agentprofile";
	
	/** The Constant AGENT_PROFILE_BACKUP. */
	public static final String AGENT_PROFILE_BACKUP = "backup";
	
	//error messages
	/** The Constant AGENT_PROF_SUCCESS. */
	public static final String AGENT_PROF_SUCCESS = "Success fully inserted/updated the agent deatils";
	
	/** The Constant ACHV_PROF_SUCCESS. */
	public static final String ACHV_PROF_SUCCESS = "Success fully inserted/updated the Achievement deatils";
	
	/** The Constant AGENT_INVALID. */
	public static final String AGENT_INVALID= "Invalid Agent ,Cannot Insert into AGENT table";
	
	/** The Constant ACHV_INVALID. */
	public static final String ACHV_INVALID= "Invalid Agent, Cannot Insert into ACHIEVEMENT table";
	
	// for file uploads
	
	/** The Constant THRESHOLD_SIZE. */
	public static final int THRESHOLD_SIZE = 1024 * 1024 * 3; // 3MB
	
	/** The Constant MAX_FILE_SIZE. */
	public static final int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
	
	/** The Constant REQUEST_SIZE. */
	public static final int REQUEST_SIZE = 1024 * 1024 * 50; // 50MB
	
	// Status messages
	
	/** The Constant AGENT_STATUS_SUCCESS. */
	public static final String AGENT_STATUS_SUCCESS = "AGENT SUCCESS";
	
	/** The Constant AGENT_STATUS_FAILURE. */
	public static final String AGENT_STATUS_FAILURE= "AGENT FAIL";
	
	/** The Constant AGENT_STATUS_INVALID. */
	public static final String AGENT_STATUS_INVALID= "AGENT INVALID";
	
	/** The Constant ACHIEVMENT_STATUS_SUCCESS. */
	public static final String ACHIEVMENT_STATUS_SUCCESS = "ACHIEVMENT SUCCESS";
	
	/** The Constant ACHIEVMENT_STATUS_FAILURE. */
	public static final String ACHIEVMENT_STATUS_FAILURE= "ACHIEVMENT FAIL";
	
	/** The Constant ACHIEVMENT_STATUS_INVALID. */
	public static final String ACHIEVMENT_STATUS_INVALID= "ACHIEVMENT INVALID";
	
	
	//Excel heading constants
	
	 /** The Constant AGENT_CODE. */
	public static final String AGENT_CODE="AGENT CODE";
	 
 	/** The Constant AGENT_GO. */
 	public static final String AGENT_GO="GO";
	 
 	/** The Constant AGENT_EMP. */
 	public static final String AGENT_EMP="EMPLOYEE ID";
	 
 	/** The Constant AGENT_FULLNAME. */
 	public static final String AGENT_FULLNAME="FULL NAME";
	 
 	/** The Constant AGENT_ROLE. */
 	public static final String AGENT_ROLE="ROLE";
	 
	 /** The Constant AGENT_ADM_CODE. */
 	public static final String AGENT_ADM_CODE="ADM CODE";
	 
 	/** The Constant AGENT_BRIEF. */
 	public static final String AGENT_BRIEF="BRIEF WRITE UP";
	 
 	/** The Constant AGENT_TRUST. */
 	public static final String AGENT_TRUST="TRUST FACTOR";
	 
 	/** The Constant AGENT_ADVISOR. */
 	public static final String AGENT_ADVISOR="ADVISOR CATEGORY";
	
	
	 /** The Constant ACHV_AGENT_CODE. */
 	public static final String ACHV_AGENT_CODE="AGENT CODE";
	 
 	/** The Constant ACHV_TITLE. */
 	public static final String ACHV_TITLE="ACHIEVEMENT ID";
	 
 	/** The Constant ACHV_TEXT. */
 	public static final String ACHV_TEXT="TEXT";
	 
	 /** The Constant DEFAULT_LANGUAGE. */
 	public static final String DEFAULT_LANGUAGE="English";
	 
	 /** The Constant CARRIER_XL. */
		public static final String CARRIER_XL = "CARRIERS";
		
		public static final String CARRIER_TBL = "CORE_CARRIER";

		/** The Constant LOOKUP_XL. */
		public static final String LOOKUP_XL = "CODE_LOOK_UP";
		
		public static final String LOOKUP_TBL = "CODE_LOOKUP";

		/** The Constant LOCALE_XL. */
		public static final String LOCALE_XL = "CODE_LOCALE_LOOKUP";
		
		public static final String LOCALE_TBL = "CODE_LOCALE_LOOKUP";

		/** The Constant RELATION_XL. */
		public static final String RELATION_XL = "CODE_RELATIONS";
		
		public static final String RELATION_TBL = "CODE_RELATION";

		/** The Constant CODE_TYPE_XL. */
		public static final String CODE_TYPE_XL = "CODE_TYPE_LOOKUP";
		
		public static final String CODE_TYPE_TBL = "CODE_TYPE_LOOKUP";
		
		/** The Constant INSERT_CARRIER. */
		public static final String INSERT_CARRIER = "le.mdu.insert.carrier";
		
		/** The Constant INSERT_CODE_TYPE_LOOKUP. */
		public static final String INSERT_CODE_TYPE_LOOKUP ="le.mdu.insert.codetypelk";
		
		/** The Constant INSERT_CODE_LOOKUP. */
		public static final String INSERT_CODE_LOOKUP = "le.mdu.insert.codelk";
		
		/** The Constant INSERT_RELATIONS. */
		public static final String INSERT_RELATIONS = "le.mdu.insert.relation";
		
		/** The Constant INSERT_LOCALE_LOOKUP. */
		public static final String INSERT_LOCALE_LOOKUP= "le.mdu.insert.locale";
		
		/** The Constant CHECK_TABLE. */
		public static final String CHECK_TABLE = "le.mdu.check.table";
		
		/** The Constant PROPERTY_FILE. */
		public static final String PROPERTY_FILE = "omni_config.properties";
		
		public static final String DBTYPE = "le.mdu.app.jdbc.dbtype";
		
		public static final String SCHEMA = "le.mdu.app.jdbc.schema";
		
		// Table Headers
		
		public static final String USER_ID = "USER ID";
		
		public static final String EMP_CODE = "EMPLOYEE CODE";
		
		public static final String NAME = "FULL NAME";
		
		public static final String EMP_TYPE = "EMPLOYEE TYPE";
		
		public static final String DESIGNATION = "DESIGNATION";
		
		public static final String SUP_CD = "SUPERVISOR CODE";
		
		public static final String OFFICE = "OFFICE";
		
		public static final String UNIT = "UNIT";
		
		public static final String GROUP = "GROUP";
		
		public static final String WRITEUP = "BRIEF WRITEUP";
		
		public static final String YOE = "YEARS OF EXPERIENCE";
		
		public static final String BUS_SRC = "BUSINESS SOURCED";
		
		public static final String NUM_SVC = "NO OF CUSTOMER SERVICING";
		
		public static final String LIC_NUM = "LICENSE NUMBER";
		
		public static final String LIC_ISS_DT = "LICENSE ISSUE DATE";
		
		public static final String LIC_EXP_DT = "LICENSE EXPIRY DATE";
		
		public static final String EMAIL = "EMAIL ID";
		
		public static final String MOB = "MOBILE NUMBER";
		
		public static final String TITLE = "TITLE";
		
		public static final String IMAGE ="IMAGE";
		
		public static final String DESCRIPTION ="DESCRIPTION";
		
		public static final String ACH_TITLE = "ACHIEVEMENT_TITLE";

		public static final String CHANNEL = "CHANNEL";
		
		public static final String ENC_ALGM ="PBEWithMD5AndDES";
		
		public static final String ENC_VAR= "LE_PBE_PWD";
		
		public static final String ENC_VAR_PART= "143112";
	
	
	
}
