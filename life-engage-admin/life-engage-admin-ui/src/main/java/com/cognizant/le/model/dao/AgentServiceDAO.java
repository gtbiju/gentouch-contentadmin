package com.cognizant.le.model.dao;

import java.util.List;
import java.util.Set;

import com.cognizant.le.model.dto.AchievementDetailsDTO;
import com.cognizant.le.model.dto.AgentAchievmentDTO;
import com.cognizant.le.model.dto.AgentDTO;

public interface AgentServiceDAO {

	public boolean isValidAgent(String agentId) throws Exception;

	public boolean isValidUser(String userid, String password) throws Exception;

	public List<AgentAchievmentDTO> insertAchievments(
			List<AgentAchievmentDTO> achievments) throws Exception;

	public Set<AgentDTO> saveToDatabase(Set<AgentDTO> agents,
			List<AgentAchievmentDTO> achievements,
			Set<AchievementDetailsDTO> achievementDtls, String mode) throws Exception;
	
	public String getChannelID(String channel);

}
