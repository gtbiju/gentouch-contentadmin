package com.cognizant.le.model.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
	@NamedQuery(
	name = "fetchId",
	query = "select id from AppContext where context = :appContext"
	),
	@NamedQuery(
	name = "fetchContexts",
	query = "from AppContext"
	)
})

@Entity
@Table(name = "APPLICATION_CONTEXTS")
//@Table(name = "APPLICATION_CONTEXTS")
public class AppContext {
	@Id
	@Column(name="Id")
	private Integer id;
	
	@Column(name="Context")
	private String context;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

}
