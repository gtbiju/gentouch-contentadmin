package com.cognizant.le.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;


/**
 * Servlet implementation class  	
 */
public class UpdateAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger LOGGER = Logger.getLogger(UpdateAction.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateAction() {
        super();
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    HttpSession session = request.getSession(true);
	    LOGGER.debug("Method Entry : UpdateAction.doGet");
	    try {
			String selectionInfo=request.getParameter("type");
			if(selectionInfo != null) {
				session.setAttribute("selectionInfo", selectionInfo);
				response.sendRedirect(""+request.getContextPath()+"/jsp/UpdateContent.jsp");
			}
	    	
		} catch (Exception e) {
			LOGGER.error("Error in UpdateAction.doGet", e);
		}
		 LOGGER.debug("Method Exit : UpdateAction.doGet");
	}
}
