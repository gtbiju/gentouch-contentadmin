<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login Page</title>

<link type="text/css" rel="stylesheet" href="css/styles_workbench.css" />
<link type="text/css" rel="stylesheet" href="css/style.css" />
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">

window.history.forward(1);
function noBack() 
{ 
window.history.forward(); 
}

function submit(){
	document.LoginForm.submit();
}

$(document).ready(function() {  
	var msg = "<%=request.getAttribute("requestMsg")%>";
	if (msg.length > 4) {
		document.getElementById('loginmsg').innerHTML=msg;
	}
	
	
	$("#password").keyup(function(event){
	    if(event.keyCode == 13){
	        $("#loginButton").click();
	    }
	});
	
});
</script>

</head>
<body class="loginBody">


	<div class="headerLogin headerStyle">
	<img src="img/logo-main.png" alt="logo-main" class="mainLogo" >
	</div>
    
   
    <div class="LoginContainer">
    <form action="Login" method="post" name="LoginForm">
      <table width="400" border="0" align="center" cellpadding="0" cellspacing="5">
        <tr>
          <td width="141">&nbsp;</td>
          <td width="244">&nbsp;</td>
        </tr>
        <tr>
          <td height="30" align="center" class="blue_heading"></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td height="30" align="center">Username&nbsp;</td>
          <td align="left">	
          <input name="username" type="text"  style="width:200px;" class="searh_box" /></td>
        </tr>
        <tr class="pdg_top">
          <td height="30" align="center" class="pdg_top">Password&nbsp;</td>
          <td align="left" class="pdg_top">
          <input name="password" type="password" id="password"  style="width:200px;" class="searh_box" /></td>
        </tr>
        <tr>
		
          <td height="30" align="center"> <div style="float: right;vertical-align: bottom;padding-top:5px;"></td>
          <td align="left"  class="pdg_top">
          <button class="btn-primary  btn-large" id="loginButton" onclick="submit()"><strong class="large">Log In</strong></button>
         </td>
        </tr>
        <tr>
        <td colspan="2" id="loginmsg" style="color:red; padding-top:10px"> </td>
        </tr>
      	</table>
      	</form>
	</div>
    <div style="height:150px; width:100%" class="cognizant_logo"></div>
</div>

</body>

</html>
