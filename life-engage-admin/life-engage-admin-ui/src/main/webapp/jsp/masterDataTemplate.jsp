<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="app" xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="../css/css_reset.css" />
<link rel="stylesheet" href="../css/styles_workbench.css" />
<title>Master Data Uploader</title>
<script type="text/javascript" src="../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/moment.min.js"></script>

<link rel="stylesheet" href="../css/bootstrap.min.css">
<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="../css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="../css/jquery.fileupload.css">
<link rel="stylesheet" href="../css/jquery.fileupload-ui.css">
<link rel="stylesheet" href="../css/jquery.dataTables.css">


<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript>
	<link rel="stylesheet" href="../css/jquery.fileupload-noscript.css">
</noscript>
<noscript>
	<link rel="stylesheet" href="../css/jquery.fileupload-ui-noscript.css">
</noscript>

<style>
.contentPosition {
	width: 30%;
	background-color: white;
	height: 165px;
	margin: auto;
	text-align: center;
}

.agent_custom_upload {
	margin: 20px;
	width: 100%;
	float: left;
	left: 50%;
}

.upload_agent_button {
	width: 75%;
}

.mand_statement_ap {
	font-size: 11px;
	padding-left: 7px;
	float: left;
}

#agent_upload_pop_up_btn {
	float: right;
	margin-right: 59px;
}
</style>
<script>
function logoutClick(){
	document.location.href="<%=request.getContextPath()%>/Logout";
}

function agentPage(){
	document.location.href="<%=request.getContextPath()%>/AgentUpload";
}
function mdbLoader(){
	document.location.href="<%=request.getContextPath()%>/jsp/masterDataTemplate.jsp";
}
function contentPage(){
	document.location.href="<%=request.getContextPath()%>/loadFile";
}
</script>
</head>
<body>
	<div class="blue_bg_header">
		<div class="top_header_components clearfix">

			<button class="logout_action_btn" id="logout_btn"
				onClick="logoutClick()">Logout</button>
		</div>
	</div>
	<div class="logo_header">
		<img src="../img/logo-main.png" alt="logo-main" class="mainLogo" ><span
			class="header_title">Master Data Uploader</span>
	</div>
	<div class="containerMain">
		<div class="containerMenu ">
			<div class="TopSlideMenu main_nav_bar nav_bg_green">
				<ul class="main_nav_bar_list clearfix">
					<!-- <li class="activeList"><a href="javascript:void(0)" rel="#showTop">About us</a></li>
					<li class="activeList"><a href="javascript:void(0)" rel="#showTop1">Services</a></li>
					<li class="activeList"><a href="javascript:void(0)" rel="#showTop2">Products</a></li> -->

					<li class="activeList"><a class="clearfix"
						onclick="agentPage()"><span
							class="menu_link_text">Users</span></a></li>
					<li class="activeList last_li"><a class="clearfix"
						onClick="contentPage()" rel="#showContent"><span
							class="menu_link_text">Content</span></a></li>
					<li class="active activeList last_li"><a class="clearfix"
						onClick="" rel="#showContent"><span
							class="menu_link_text">Master Data</span></a></li>

				</ul>
			</div>
			<div class="submenu_container slide-menu-top" id="showProducts">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li class="activated"><a href="#" title="menu">Application
									innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>
							<li><a href="#" title="menu">Business strategy</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>

			<div class="submenu_container slide-menu-top" id="showUsers">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>
							<li><a href="#" title="menu">Business strategy</a></li>

						</ul>
					</div>
				</div>
				<div class="submenu_column2">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>
							<li><a href="#" title="menu">Business strategy</a></li>

						</ul>
					</div>
				</div>
				<div class="clear"></div>
			</div>

			<div class="submenu_container slide-menu-top" id="showContent">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li class="activated"><a href="#" title="menu">
									Add/Update Content </a></li>
						</ul>
					</div>
				</div>

				<div class="clear"></div>
			</div>
			<div class="submenu_container slide-menu-top" id="showApp">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>
			<div class="submenu_container slide-menu-top" id="showRules">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>
			<div class="submenu_container slide-menu-top" id="showCalcs">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>
		</div>
		<div class="clear"></div>
	</div>

	<div class="bg_white"
		style="height: 35px; border-bottom: none; background-color: #f5f5f5 !important;">
		<ul class="breadcrumb breadcrumb_gradientbg">
			<li><a href="#" class="brd_link_sel">Users</a></li>
		</ul>
	</div>

	<!-- CONTENT CONTAINER -->
	<div class="content_container">

		<div class="container_tlr_fluid">
			<div class="blankContainer">
				<div class="blankContainerInner">
					<div class="table_container">

						<div class="file_upload_container"
							style="width: 85%; border: 1px; padding: 20px; margin: 20px 0; border: 1px solid #c32a24; border-left-width: 5px; border-radius: 3px; border-left-color: #c32a24; margin-left: 5%; background-color: #fff;">
							<h4
								style="color: slategrey; margin-top: 0; margin-bottom: 5px; font-size: 18px;">Select
								the Master Data Template</h4>
							<form id="fileupload" action="mdb/upload" method="POST"
								enctype="multipart/form-data">
								<!-- Redirect browsers with JavaScript disabled to the origin page -->
								<noscript>
									<input type="hidden" name="redirect"
										value="http://blueimp.github.io/jQuery-File-Upload/">
								</noscript>
								<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
								<div class="row fileupload-buttonbar">
									<div class="col-lg-7">
										<!-- The fileinput-button span is used to style the file input field as button -->
										<span class="btn btn-success selBtnCustom fileinput-button"> <i
											class="glyphicon glyphicon-plus"></i> <span>Select
												file</span> <input type="file" name="files[]" data-url="test">
										</span>

										<!-- The global file processing state -->
										<span class="fileupload-process"></span>
									</div>
								</div>
								<!-- The table listing the files available for upload/download -->
								<table role="presentation" class="table table-striped" id="presentation-table">
									<tbody class="files"></tbody>
								</table>
							</form>


						</div>

					</div>

				</div>
			</div>
		</div>

	</div>

	<!-- CONTENT CONTAINER -->



	<div class="footer_container">
		<span class="copyright_label">&copy;Copyright 2013. LifeEngage
		</span>
	</div>


	<div id="page_loader"></div>


	<div id="popupDiv" class="popup popup2" style="display: none;">
		<div class="popupHeading">
			<a onclick="closePopup()">close</a>
		</div>
		<div class="popupContent">Invalid file name</div>
		<div class="shadow"></div>
		<div class="btn">
			<span> <input type="button" id="closeDiv" class="orangeButton"
				value="OK" onclick="closePopup()" />
			</span>
		</div>
	</div>

	<!-- THE MODAL AGENTUPLOADRESULT CONTENT POPUP -->





	<!-- Modal HTML -->
	<div id="myModal" class="modal fade">
		<div class="modal-dialog" style="width: 60%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Result</h4>
				</div>
				<div class="modal-body" style="min-height:370px;">
					<div id="sidebar" class="list-group" style="width:30%; float:left;min-height:140px;">

					</div>
				
					<div class="list-group" style="width:65%; float:right; min-height:140px;">
						    <table id="detail_table" class="table table-hover" style=" border: 1px solid #ddd;">
							
							</table>
					</div>
				</div>	

				
				<div style="width:100%; height 10px;"> </div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<a href="mdb/download" id="download_sqlite" type="button" class="btn btn-primary">Download</a>
				</div>
			</div>
		</div>
	</div>
	
	
	<div id="messageModal" class="modal fade">
	<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Error</h4>
				</div>
				<div id="error-body" class="modal-body">
				</div>
			</div>	
	</div>
	</div>






	<!-- The template to display files available for upload -->
	<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
	<!-- The template to display files available for download -->
	<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
	<script src="../js/jquery-2.1.1.min.js"></script>
	<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
	<script src="../js/vendor/jquery.ui.widget.js"></script>
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="../js/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="../js/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="../js/canvas-to-blob.min.js"></script>
	<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
	<script src="../js/bootstrap.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="../js/jquery.blueimp-gallery.min.js"></script>
	<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="../js/jquery.iframe-transport.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery.fileupload.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="../js/jquery.fileupload-process.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="../js/jquery.fileupload-image.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="../js/jquery.fileupload-audio.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="../js/jquery.fileupload-video.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="../js/jquery.fileupload-validate.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="../js/jquery.fileupload-ui.js"></script>
	<!-- The main application script -->
	<script src="../js/uploadfunction.js"></script>
	<script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
	<script src="//cdn.datatables.net/plug-ins/725b2a2115b/pagination/ellipses.js"></script>

</body>
</html>
