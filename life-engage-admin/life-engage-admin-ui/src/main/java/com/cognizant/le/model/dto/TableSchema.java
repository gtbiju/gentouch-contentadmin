/**
 *
 * � Copyright 2012, Cognizant
 *
 * @author        : 287304
 * @version       : 0.1, May 16, 2014
 */
package com.cognizant.le.model.dto;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class TableSchema.
 */
public class TableSchema {

    /** The table name. */
    private String tableName;

    /** The table schema name. */
    private String tableSchemaName;

    /** The columns. */
    private List<ColumnSchema> columns;

    /** The primary key. */
    private List<String> primaryKey;

	/** The foreign keys. */
	private List<ForeignKeySchema> foreignKeys;

    /** The indexes. */
    private List<IndexSchema> indexes;

	/**
	 * Gets the table name.
	 *
	 * @return the table name
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * Sets the table name.
	 *
	 * @param tableName the new table name
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * Gets the table schema name.
	 *
	 * @return the table schema name
	 */
	public String getTableSchemaName() {
		return tableSchemaName;
	}

	/**
	 * Sets the table schema name.
	 *
	 * @param tableSchemaName the new table schema name
	 */
	public void setTableSchemaName(String tableSchemaName) {
		this.tableSchemaName = tableSchemaName;
	}

	/**
	 * Gets the columns.
	 *
	 * @return the columns
	 */
	public List<ColumnSchema> getColumns() {
		return columns;
	}

	/**
	 * Sets the columns.
	 *
	 * @param columns the new columns
	 */
	public void setColumns(List<ColumnSchema> columns) {
		this.columns = columns;
	}

	/**
	 * Gets the primary key.
	 *
	 * @return the primary key
	 */
	public List<String> getPrimaryKey() {
		return primaryKey;
	}

	/**
	 * Sets the primary key.
	 *
	 * @param primaryKey the new primary key
	 */
	public void setPrimaryKey(List<String> primaryKey) {
		this.primaryKey = primaryKey;
	}

	/**
	 * Gets the foreign keys.
	 *
	 * @return the foreign keys
	 */
	public List<ForeignKeySchema> getForeignKeys() {
		return foreignKeys;
	}

	/**
	 * Sets the foreign keys.
	 *
	 * @param foreignKeys the new foreign keys
	 */
	public void setForeignKeys(List<ForeignKeySchema> foreignKeys) {
		this.foreignKeys = foreignKeys;
	}

	/**
	 * Gets the indexes.
	 *
	 * @return the indexes
	 */
	public List<IndexSchema> getIndexes() {
		return indexes;
	}

	/**
	 * Sets the indexes.
	 *
	 * @param indexes the new indexes
	 */
	public void setIndexes(List<IndexSchema> indexes) {
		this.indexes = indexes;
	}

}
