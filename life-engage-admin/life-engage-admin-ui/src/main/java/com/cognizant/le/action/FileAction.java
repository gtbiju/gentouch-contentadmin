package com.cognizant.le.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cognizant.le.manager.AgentServiceManager;
import com.cognizant.le.manager.AgentServiceManagerImpl;
import com.cognizant.le.model.dto.AchievementDetails;
import com.cognizant.le.model.dto.AchievementDetailsDTO;
import com.cognizant.le.model.dto.AgentAchievmentDTO;
import com.cognizant.le.model.dto.AgentDTO;
import com.cognizant.le.model.dto.AgentProfile;
import com.cognizant.le.util.Constants;
import com.cognizant.le.util.LoggerUtil;

/**
 * Servlet implementation class FileAction
 */
public class FileAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(FileAction.class);
	
	List<String> agentCodes = new ArrayList<String>();
	
	List<String> achTitles = new ArrayList<String>();
	
	private String opMode = "";

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	public void doPost1(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.debug(LoggerUtil.getLogMessage(
				"Method Entry :FileAction.Browsw  for Agent:", ""));
		response.setContentType("text/html");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		String fileName = "";
		AgentProfile profile = null;
		try {
			fileName = uploadFile(request);

			List<AgentDTO> dataHolder1 = null;
			List<AgentAchievmentDTO> dataHolder2 = null;
			List<AchievementDetailsDTO> dataHolder3 = null;
			if (fileName != null && !fileName.isEmpty()) {
				String fileExtension = getFileExtension(fileName);
				if ((fileExtension.contains("xls"))
						|| (fileExtension.contains("xlsx"))
						|| (fileExtension.contains("XLS"))
						|| (fileExtension.contains("XLSX"))) {
					boolean agentValid = false;
					boolean achvValid = false;

					Workbook wb_xssf;
					Sheet achDtlSheet = null;
					String fileExtn = getFileExtension(fileName);
					if (fileExtn.equalsIgnoreCase("xlsx")) {
						wb_xssf = new XSSFWorkbook(fileName);
						achDtlSheet = wb_xssf.getSheetAt(2);
					} else if (fileExtn.equalsIgnoreCase("xls")) {
						FileInputStream myInput = new FileInputStream(fileName);
						POIFSFileSystem myFileSystem = new POIFSFileSystem(
								myInput);
						wb_xssf = new HSSFWorkbook(myFileSystem);
						achDtlSheet = wb_xssf.getSheetAt(0);
					}

					boolean achDtlValid = validateAchievementDetails(achDtlSheet);

					if ((!agentValid) && (!achvValid) && (!achDtlValid)) {
						Map<String, List<AgentDTO>> agentMap = new HashMap<String, List<AgentDTO>>();
						dataHolder1 = agentMap.get("VALID");
/*						profile = uploadAgentToDB(dataHolder1, dataHolder2,
								dataHolder3);*/
						List<AgentDTO> invalidDTO = agentMap.get("INVALID");
						profile.setInvalidAgent(profile.getInvalidAgent()
								+ invalidDTO.size());
						profile.setTotalAgents(profile.getTotalAgents()
								+ invalidDTO.size());
						profile.getAgents().addAll(invalidDTO);
					} else {
						profile = new AgentProfile();
						profile.setAgentValid(agentValid);
						profile.setAchvValid(achvValid);

					}
					logger.debug(LoggerUtil.getLogMessage(
							"*Log Entry Agent Profile and Agent Datas :", ""));
					if (profile != null) {
						logger.debug(LoggerUtil.getLogMessage(
								"Agent Profile Datas :", profile + ""));
					}
					if (dataHolder1 != null) {
						for (AgentDTO agent : dataHolder1) {
							logger.debug(LoggerUtil.getLogMessage(
									"Agent's Datas:", agent + ""));
						}
					}
					logger.debug(LoggerUtil.getLogMessage(
							"*Log Exit Agent Profile and Agent Datas :", ""));
					request.setAttribute("profile", profile);
					// archieve files
					// moveFiles(fileName);

					RequestDispatcher view = request
							.getRequestDispatcher("/jsp/agentprofileupload.jsp");

					view.include(request, response);
					logger.debug(LoggerUtil.getLogMessage(
							"Method End :FileAction.Browse  for Agent:", ""));

				}
			}

		}

		catch (Exception e) {
			logger.error(LoggerUtil.getLogMessage(
					"Error while FileAction.Browse:", "public exception"), e);
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}



	private String uploadFile(HttpServletRequest request)
			throws ServletException, IOException {

		String fullFileName = null;
		String fileName = null;
		// check if the request actually contains a file
		if (!ServletFileUpload.isMultipartContent(request)) {
			// if not, we stop here
			return "error";
		}
		// configures some settings
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(Constants.THRESHOLD_SIZE);
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setFileSizeMax(Constants.MAX_FILE_SIZE);
		upload.setSizeMax(Constants.REQUEST_SIZE);
		Properties mailProperties = new Properties();
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream stream = loader.getResourceAsStream("/"
				+ Constants.PROPERTY_FILE);
		if (stream == null) {
			stream = loader.getResourceAsStream(Constants.PROPERTY_FILE);
		}
		try {
			mailProperties.load(stream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Set the PDF path
		String serverPath = mailProperties
				.getProperty(Constants.AGENT_PROFILE_PATH);
		// creates the directory if it does not exist
		File uploadDir = new File(serverPath);
		if (!uploadDir.exists()) {
			uploadDir.mkdir();
		}
		try {
			// parses the request's content to extract file data
			List formItems = upload.parseRequest(request);
			Iterator iter = formItems.iterator();

			// iterates over form's fields
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();
				// processes only fields that are not form fields

				if (!item.isFormField()) {
					fileName = new File(item.getName()).getName();
					Calendar lCDateTime = Calendar.getInstance();
					Long timeInMS = lCDateTime.getTimeInMillis();
					fileName = timeInMS + "_" + fileName;
					fullFileName = serverPath + File.separator + fileName;

					File storeFile = new File(fullFileName);
					// saves the file on disk
					item.write(storeFile);
				}
			}

			// AgentFileSyncUtility.refreshDataBase();
		} catch (Exception ex) {
			request.setAttribute("message",
					"There was an error: " + ex.getMessage());
		}
		return fullFileName;

	}



	private AgentProfile uploadAgentToDB (AgentProfile agentProfile) throws Exception {
		AgentProfile results = null;
		AgentServiceManager service = new AgentServiceManagerImpl();
		try {

			results = service.saveAgentProfile(agentProfile,opMode);
		} catch (Exception e) {

			logger.error(LoggerUtil.getLogMessage(
					"Error while FileAction.uploadAgentToDB",
					"public exception"), e);
		}

		return results;
	}

	private String getFileExtension(String fname2) {
		String fileName = fname2;
		String fname = "";
		String ext = "";
		int mid = fileName.lastIndexOf(".");
		fname = fileName.substring(0, mid);
		ext = fileName.substring(mid + 1, fileName.length());
		return ext;
	}



	public String returnStringCell(Cell dataCell) {
		String value = null;
		if (dataCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			value = String.valueOf((long) dataCell.getNumericCellValue());

		} else if (dataCell.getCellType() == Cell.CELL_TYPE_STRING) {

			value = dataCell.getStringCellValue();
		}
		return value;
	}

	public Long returnNumericCell(Cell dataCell) {
		Double value = null;
		if (dataCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			value = dataCell.getNumericCellValue();

		} else if (dataCell.getCellType() == Cell.CELL_TYPE_STRING) {

			value = Double.parseDouble(dataCell.getStringCellValue());
		}
		return value.longValue();
	}

	public Date returnDateCell(Cell dataCell) {
		Date value = null;
		if (DateUtil.isCellDateFormatted(dataCell)) {
			value = dataCell.getDateCellValue();
		}
		return value;
	}

	private Boolean validateAchievementDetails(Sheet sheet) {

		Boolean isNotValidSheet = false;
		if (null != sheet) {
			Iterator rows = sheet.rowIterator();
			Map<Integer, String> headerMap = new HashMap<Integer, String>();
			headerMap.put(0, Constants.TITLE);
			headerMap.put(1, Constants.IMAGE);
			headerMap.put(2, Constants.DESCRIPTION);
			if (rows.hasNext()) {
				Row myRow = (Row) rows.next();
				Iterator cellIter = myRow.cellIterator();
				while (cellIter.hasNext()) {
					Cell myCell = (Cell) cellIter.next();

					if (headerMap.containsKey(myCell.getColumnIndex())) {
						if (!(returnStringCell(myCell)).trim()
								.equalsIgnoreCase(
										headerMap.get(myCell.getColumnIndex()))) {
							isNotValidSheet = true;
							return isNotValidSheet;
						}
					} else {
						break;
					}

				}
			}
		}
		return isNotValidSheet;
	}

	private Map<String, Set<AchievementDetailsDTO>> readAchievementDetails(
			Sheet sheet) {
		Map<String, Set<AchievementDetailsDTO>> resultMap = null;
		achTitles = new ArrayList<String>();
		if (null != sheet) {
			resultMap = new HashMap<String, Set<AchievementDetailsDTO>>();
			Set<AchievementDetailsDTO> validAchievementDtls = new HashSet<AchievementDetailsDTO>();
			Set<AchievementDetailsDTO> invalidAchievementDtls = new HashSet<AchievementDetailsDTO>();
			Iterator rows_achiev = sheet.rowIterator();
			while (rows_achiev.hasNext()) {
				AchievementDetailsDTO agentAchievmentDtl = new AchievementDetailsDTO();
				Row myRow = (Row) rows_achiev.next();
				if (myRow.getRowNum() == 0) {
					continue;
				}
				if (myRow.getRowNum() > 0) {
					Iterator cellIter = myRow.cellIterator();
					while (cellIter.hasNext()) {
						Cell myCell = (Cell) cellIter.next();
						if (myCell.getColumnIndex() == 0) {
							agentAchievmentDtl
									.setTitle(returnStringCell(myCell));
							achTitles.add(agentAchievmentDtl.getTitle());
						} else if (myCell.getColumnIndex() == 1) {
							agentAchievmentDtl
									.setImage(returnStringCell(myCell));
						} else if (myCell.getColumnIndex() == 2) {
							agentAchievmentDtl
									.setDescription(returnStringCell(myCell));
						} else {
							break;
						}

					}
				}
				if (!validAchievementDtls.add(agentAchievmentDtl)) {
					invalidAchievementDtls.add(agentAchievmentDtl);
				}
			}
			resultMap.put("VALID", validAchievementDtls);
			resultMap.put("INVALID", invalidAchievementDtls);
		}
		return resultMap;
	}

	private Boolean validateAgentHeaders(Sheet sheet) {
		Boolean valid = true;
		if (null != sheet) {
			Iterator rows = sheet.rowIterator();
			Map<Integer, String> headerMap = new HashMap<Integer, String>();
			headerMap.put(0, Constants.USER_ID);
			headerMap.put(1, Constants.EMP_CODE);
			headerMap.put(2, Constants.NAME);
			headerMap.put(3, Constants.EMP_TYPE);
			headerMap.put(4, Constants.DESIGNATION);
			headerMap.put(5, Constants.SUP_CD);
			headerMap.put(6, Constants.OFFICE);
			headerMap.put(7, Constants.UNIT);
			headerMap.put(8, Constants.GROUP);
			headerMap.put(9, Constants.WRITEUP);
			headerMap.put(10, Constants.YOE);
			headerMap.put(11, Constants.BUS_SRC);
			headerMap.put(12, Constants.NUM_SVC);
			headerMap.put(13, Constants.LIC_NUM);
			headerMap.put(14, Constants.LIC_ISS_DT);
			headerMap.put(15, Constants.LIC_EXP_DT);
			headerMap.put(16, Constants.EMAIL);
			headerMap.put(17, Constants.MOB);
			headerMap.put(18, Constants.CHANNEL);
			if (rows.hasNext()) {
				Row myRow = (Row) rows.next();
				Iterator cellIter = myRow.cellIterator();
				valid = validateHeader(myRow, headerMap);
			} else {
				valid = false;
			}
		} else {
			valid = false;
		}
		return valid;

	}

	private Boolean validateAchievementHeaders(Sheet sheet) {
		Boolean valid = true;
		if (null != sheet) {
			Iterator rows = sheet.rowIterator();
			Map<Integer, String> headerMap = new HashMap<Integer, String>();
			headerMap.put(0, Constants.EMP_CODE);
			headerMap.put(1, Constants.ACH_TITLE);
			if (rows.hasNext()) {
				Row myRow = (Row) rows.next();
				valid = validateHeader(myRow, headerMap);
			} else {
				valid = false;
			}
		} else {
			valid = false;
		}
		return valid;
	}

	private Boolean validateAchievementDetailsHeader(Sheet sheet) {
		Boolean valid = true;
		if (null != sheet) {
			Iterator rows = sheet.rowIterator();
			Map<Integer, String> headerMap = new HashMap<Integer, String>();
			headerMap.put(0, Constants.TITLE);
			headerMap.put(1, Constants.IMAGE);
			headerMap.put(2, Constants.DESCRIPTION);
			if (rows.hasNext()) {
				Row myRow = (Row) rows.next();
				valid = validateHeader(myRow, headerMap);
			}
		} else {
			valid = false;
		}
		return valid;
	}

	private Map<String, Set<AgentDTO>> readAgent(Sheet sheet) {
		agentCodes = new ArrayList<String>();
		Map<String, Set<AgentDTO>> agentsMap = null;
		if (null != sheet) {
			agentsMap = new HashMap<String, Set<AgentDTO>>();
			Iterator rows = sheet.rowIterator();
			Set<AgentDTO> validAgents = new HashSet<AgentDTO>();
			Set<AgentDTO> invalidAgents = new HashSet<AgentDTO>();
			int count = 0;
			while (rows.hasNext()) {
				AgentDTO agent = new AgentDTO();
				Row myRow = (Row) rows.next();
				count++;
				if (myRow.getRowNum() == 0) {
					continue;
				}
				if (myRow.getRowNum() > 0) {
					Iterator cellIter = myRow.cellIterator();
					cellLoop: while (cellIter.hasNext()) {
						Cell myCell = (Cell) cellIter.next();
						switch (myCell.getColumnIndex()) {
						case 0:
							agent.setUserid(returnStringCell(myCell));
							break;
						case 1:
							agent.setAgentCode(returnStringCell(myCell));
							agentCodes.add(agent.getAgentCode());
							break;
						case 2:
							agent.setFullname(returnStringCell(myCell));
							break;
						case 3:
							agent.setEmployeeType(returnStringCell(myCell));
							break;
						case 4:
							agent.setRole(returnStringCell(myCell));
							break;
						case 5:
							agent.setSupervisorCode(returnStringCell(myCell));
							break;
						case 6:
							agent.setOffice(returnStringCell(myCell));
							break;
						case 7:
							agent.setUnit(returnStringCell(myCell));
							break;
						case 8:
							agent.setGroup(returnStringCell(myCell));
							break;
						case 9:
							agent.setBriefWriteUp(returnStringCell(myCell));
							break;
						case 10:
							agent.setYearsOfExperience(returnNumericCell(myCell)
									.intValue());
							break;
						case 11:
							agent.setBusinessSourced(returnNumericCell(myCell));
							break;
						case 12:
							agent.setNumOfCustServiced(returnNumericCell(myCell));
							break;
						case 13:
							agent.setLicenseNumber(returnStringCell(myCell));
							break;
						case 14:
							agent.setLicenseIssueDate(returnDateCell(myCell));
							break;
						case 15:
							agent.setLicenseExpiryDate(returnDateCell(myCell));
							break;
						case 16:
							agent.setEmailId(returnStringCell(myCell));
							break;
						case 17:
							agent.setMobileNumber(returnStringCell(myCell));
							break;
						case 18:
							agent.setChannel(returnStringCell(myCell));
							break;
						default:
							break cellLoop;
						}

					}// while
					agent.setRowNumber(count);
				}// IF

				boolean validChannel = validateChannel(agent);
				
				if (agent.getAgentCode() != null && agent.getUserid() != null
						&& agent.getFullname() != null) {

					if (!validAgents.add(agent)) {
						agent.setStatus(Constants.AGENT_STATUS_INVALID);
						agent.setMessage("Error at Row : "+agent.getRowNumber()+" Duplicate Agent Code : "+agent.getAgentCode());
						invalidAgents.add(agent);
					}else if (!validChannel){
						//removes the agent from valid set. Agent added to set in the first place to check for duplicate code.
						validAgents.remove(agent);
					}
					if(!validChannel){
						agent.setStatus(Constants.AGENT_STATUS_INVALID);
						if(agent.getMessage()!=null && agent.getMessage()!=""){
							agent.setMessage(agent.getMessage().concat(" Error at Row : "+agent.getRowNumber()+" Invalid channel : "+agent.getChannel() ));
						}else{
							agent.setMessage("Error at Row : "+agent.getRowNumber()+" Invalid channel : "+agent.getChannel());

						}
						invalidAgents.add(agent);
					}

				} else {
					if (agent.getAgentCode() == null
							&& agent.getUserid() == null
							&& agent.getFullname() == null) {
						// ignore
					} else {
						if (agent.getUserid() == null
								|| (null != agent.getUserid() && agent
										.getUserid().trim().equals(""))) {
							agent.setStatus(Constants.AGENT_STATUS_INVALID);
							agent.setMessage("Error at Row : "+agent.getRowNumber()+" User id field is mandatory!");
						}

						if (agent.getAgentCode() == null
								|| (null != agent.getAgentCode() && agent
										.getAgentCode().trim().equals(""))) {
							agent.setStatus(Constants.AGENT_STATUS_INVALID);
							if (agent.getMessage() != null) {
								agent.setMessage(agent.getMessage().concat(
										"\n Employee Code field is mandatory!"));
							} else {
								agent.setMessage("Error at Row : "+agent.getRowNumber()+" Employee Code field is mandatory!");
							}
							// vFailAgentList.add(agent);
						}

						if (agent.getFullname() == null
								|| (null != agent.getFullname() && agent
										.getFullname().trim().equals(""))) {
							agent.setStatus(Constants.AGENT_STATUS_INVALID);
							if (agent.getMessage() != null) {
								agent.setMessage(agent.getMessage().concat(
										"\n Full Name field is mandatory!"));
							} else {
								agent.setMessage("Error at Row : "+agent.getRowNumber()+" Full Name field is mandatory!");
							}
							// vFailAgentList.add(agent);
						}

						invalidAgents.add(agent);
					}
				}
			}
			agentsMap.put("VALID", validAgents);
			agentsMap.put("INVALID", invalidAgents);
		}
		return agentsMap;
	}


	private Boolean validateHeader(Row myRow,
			Map<Integer, String> headerMap) {
			if (myRow != null) {
				if (myRow.getRowNum() == 0) {
					Iterator cellIter = myRow.cellIterator();
					while (cellIter.hasNext()) {
						Cell myCell = (Cell) cellIter.next();	
						if (headerMap.containsKey(myCell.getColumnIndex())) {
							if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(
								headerMap.get(myCell.getColumnIndex()))) {
								return false;
							}
						} else {
						// map has keys till 17 hardcoded. breaks on the next index
							break;
						}
					}
	
				} else {
				// log error
				}
		}
			return true;
	}

	public void handleFileUpload(HttpServletRequest request)
			throws ServletException, IOException {
		validateExcelSheet(request);
	}

	private AgentProfile validateExcelSheet(HttpServletRequest request)
			throws ServletException, IOException {
		String fileName = uploadFile(request);
		AgentProfile agentProfile = new AgentProfile();
		agentProfile.setMessages(new ArrayList<String>());
		String extension = getFileExtension(fileName);
		String statusMessage;
		Workbook wb_xssf = null;
		Sheet achDtlSheet;
		Sheet agentSheet;
		Sheet achievementSheet;
		if (fileName != null && !fileName.isEmpty()) {
			if (extension.equalsIgnoreCase("xlsx")
					|| extension.equalsIgnoreCase("xls")) {
				if (extension.equalsIgnoreCase("xlsx")) {
					wb_xssf = new XSSFWorkbook(fileName);
				} else if (extension.equalsIgnoreCase("xls")) {
					FileInputStream myInput = new FileInputStream(fileName);
					POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
					wb_xssf = new HSSFWorkbook(myFileSystem);
				}
				if (wb_xssf.getNumberOfSheets() >= 3) {
					agentSheet = wb_xssf.getSheetAt(0);
					achievementSheet = wb_xssf.getSheetAt(1);
					achDtlSheet = wb_xssf.getSheetAt(2);
					boolean agentValid = validateAgentHeaders(agentSheet);
					boolean achvValid = validateAchievementHeaders(achievementSheet);
					boolean achDtlValid = validateAchievementDetailsHeader(achDtlSheet);
					
					if(!(opMode.equals("REPLACE")) && !(opMode.equals("BACKDATE"))){
						agentProfile
						.getMessages()
						.add("Error : Invalid property defined - MODES Supported : REPACE | BACKDATE");
					}

					if (!agentValid) {
						agentProfile
								.getMessages()
								.add("Error : Invalid Agent Sheet - Incorrect Headers");
					}
					if (!achvValid) {
						agentProfile
								.getMessages()
								.add("Error : Invalid Achievement Sheet - Incorrect Headers");
					}
					if (!achDtlValid) {
						agentProfile
								.getMessages()
								.add("Error : Invalid Achievement Details Sheet - Incorrect Header");
					}

					Map<String, Set<AgentDTO>> agentsMap = readAgent(agentSheet);
					Map<String, Set<AchievementDetailsDTO>> achievementDtlsMap = readAchievementDetails(achDtlSheet);
					Map<String,List<AgentAchievmentDTO>> achievementsMap = readAchievement(achievementSheet);
					

					agentProfile.setAchievementDtlsMap(achievementDtlsMap);
					agentProfile.setAchievementsMap(achievementsMap);
					agentProfile.setAgentsMap(agentsMap);

					if (agentsMap.get("INVALID").size() > 0
							|| achievementDtlsMap.get("INVALID").size() > 0 ||achievementsMap.get("INVALID").size()>0) {
						if (agentsMap.get("INVALID").size() > 0) {
							for (AgentDTO agentDTO : agentsMap.get("INVALID")) {
								agentProfile.getMessages().add(
										agentDTO.getMessage());
							}
						}
						if (achievementsMap.get("INVALID").size() > 0) {
							for (AgentAchievmentDTO agentAchievmentDTO : achievementsMap.get("INVALID")) {
								if(null!=agentAchievmentDTO.getMessage()){
								agentProfile.getMessages().add(
										agentAchievmentDTO.getMessage());
								}
							}
						}
						if (achievementDtlsMap.get("INVALID").size() > 0) {
							for (AchievementDetailsDTO achDetailsDTO : achievementDtlsMap
									.get("INVALID")) {
								agentProfile.getMessages()
										.add("Duplicate "
												+ achDetailsDTO.getTitle());
							}
						}

					}

				} else {
					statusMessage = "Error : Invalid Agent Sheet - Missing sheets";
					agentProfile.getMessages().add(statusMessage);
					logger.error(LoggerUtil
							.getLogMessage("Error : Invalid Agent Sheet - Missing sheets"));
				}
			} else {
				statusMessage = "Error : Invalid Extension";
				agentProfile.getMessages().add(statusMessage);
				logger.error(LoggerUtil
						.getLogMessage("Error : Invalid Extension"));
			}
		} else {
			logger.error(LoggerUtil
					.getLogMessage("Error : File Name is null or empty"));
			statusMessage = "Error : File Name is null or empty";
			agentProfile.getMessages().add(statusMessage);
		}
		return agentProfile;
	}

	private Map<String,List<AgentAchievmentDTO>> readAchievement(Sheet achievementSheet) {
		Iterator rows_achiev = achievementSheet.rowIterator();
		Map<String,List<AgentAchievmentDTO>> achievementsMap = new HashMap<String, List<AgentAchievmentDTO>>();
		List<AgentAchievmentDTO> validAchievementSet = new ArrayList<AgentAchievmentDTO>();
		List<AgentAchievmentDTO> invalidAchievementSet = new ArrayList<AgentAchievmentDTO>();
		while (rows_achiev.hasNext()) {
			AgentAchievmentDTO agentAchievment = new AgentAchievmentDTO();
			Row myRow = (Row) rows_achiev.next();

			if (myRow.getRowNum() == 0) {
				continue;
			}
			if (myRow.getRowNum() > 0) {
				Iterator cellIter = myRow.cellIterator();
				while (cellIter.hasNext()) {
					Cell myCell = (Cell) cellIter.next();

					if (myCell.getColumnIndex() == 0) {
						String agentCode = returnStringCell(myCell);
						if (null == agentCode) {
							break;
						}
						if(agentCodes.contains(agentCode)){
							agentAchievment.setAgentCode(agentCode);
						}else{
							agentAchievment.setStatus(Constants.AGENT_STATUS_INVALID);
							agentAchievment.setMessage("Error : Orphan Achievement. Invalid reference to agentCode "+agentCode+ "at row :"+myRow.getRowNum());
						}
					}

					if (myCell.getColumnIndex() == 1) {
						String title = returnStringCell(myCell);
						agentAchievment.setTitleRef(title);
					}
				}
				if(agentAchievment.getAgentCode()!=null && agentAchievment.getStatus()!=null && !agentAchievment.getStatus().equals(Constants.AGENT_STATUS_INVALID)){
					validAchievementSet.add(agentAchievment);
				}else{
					invalidAchievementSet.add(agentAchievment);
				}
			}
		}
		achievementsMap.put("VALID", validAchievementSet);
		achievementsMap.put("INVALID", invalidAchievementSet);
		return achievementsMap;
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.debug(LoggerUtil.getLogMessage(
				"Method Entry :FileAction.Browsw  for Agent:", ""));
		response.setContentType("text/html");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		AgentProfile profile = null;
		try {
			profile = validateExcelSheet(request);
			if(profile.getMessages().size()>0){
				profile.setAchvValid(true);
				profile.setAgentValid(true);
			}else{
				profile= uploadAgentToDB(profile);
			}
			request.setAttribute("profile", profile);
			RequestDispatcher view = request
					.getRequestDispatcher("/jsp/agentprofileupload.jsp");
			view.include(request, response);
			logger.debug(LoggerUtil.getLogMessage(
					"Method End :doPost  for FileAction:", ""));

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
	}
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		logger.debug("Method Entry : FileAction.init");
		super.init(config);
		String propertiesFile = getServletContext().getInitParameter("CONFIG_PROPERTIES");
		Properties properties = new Properties();
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream stream = classLoader.getResourceAsStream("/" + propertiesFile);
			if(null == stream) {
				stream = classLoader.getResourceAsStream(propertiesFile);
			}
			properties.load(stream);			
			opMode = properties.getProperty("OPERATION_MODE");
			if((null==opMode) || "".equals(opMode.trim())) {
				logger.error("Property SHARED_CONTENT_FOLDER is not defined");
			}else{
				opMode= opMode.toUpperCase();
			}
		} catch (IOException e) {
			throw new ServletException(e.getMessage(), e);
		}
	}
	
	private Boolean validateChannel(AgentDTO agent) {
		String channelName = agent.getChannel();
		String channelId = null;
		boolean valid = false;
		AgentServiceManager agentManager = new AgentServiceManagerImpl();
		if(null != channelName){
			channelName = channelName.trim();
			channelId = agentManager.getChannelIDForName(channelName);
			if(null!=channelId){
				agent.setChannelId(channelId);
				valid = true;
			}
		}
		return valid;
	}


}
