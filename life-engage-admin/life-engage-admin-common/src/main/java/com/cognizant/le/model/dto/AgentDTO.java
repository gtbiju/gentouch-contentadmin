package com.cognizant.le.model.dto;

import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class AgentDTO.
 */
public class AgentDTO {
	
	/** The agent code. */
	private String employeeCode;	
	
	/** The employee id. */
	private String employeeId;
	
	/** The fullname. */
	private String fullname;
	
	/** The Agent Designation. */
	private String role;
	
	/** The adm code. */
	private String admCode;
	
	/** The brief write up. */
	private String briefWriteUp;
	
	/** The trust factor. */
	private String trustFactor;
	
	/** The advisor category. */
	private String advisorCategory;
	
	/** The go. */
	private String go;
	
	/** The status. */
	private String status;
	
	/** The message. */
	private String message;
	
	/** The userid. */
	private String userid;
	
	/** The uploaded_timestamp. */
	private String uploaded_timestamp;
	
	/** The name_of_excelsheet. */
	private String name_of_excelsheet;
	
	
	/** The employee type. */
	private String employeeType;
	
	/** The supervisor code. */
	private String supervisorCode;
	
	/** The office. */
	private String office;
	
	/** The unit. */
	private String unit;
	
	/** The group. */
	private String group;
	
	/** The years of experience. */
	private int yearsOfExperience;
	
	/** The business sourced. */
	private Long businessSourced;
	
	/** The num of cust serviced. */
	private Long numOfCustServiced;
	
	/** The license number. */
	private String licenseNumber;
	
	/** The license issue date. */
	private Date licenseIssueDate;
	
	/** The license expiry date. */
	private Date licenseExpiryDate;
	
	/** The email id. */
	private String emailId;
	
	/** The mobile number. */
	private String mobileNumber;
	
	/** The achievements. */
	private List<AgentAchievmentDTO> achievements; 
	
	/** The row number. */
	private int rowNumber;
	
	/** The channel. */
	private String channel;
	
	private String channelId;
	
	
	/**
	 * Gets the userid.
	 *
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}
	
	/**
	 * Sets the userid.
	 *
	 * @param userid the new userid
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	/**
	 * Gets the uploaded_timestamp.
	 *
	 * @return the uploaded_timestamp
	 */
	public String getUploaded_timestamp() {
		return uploaded_timestamp;
	}
	
	/**
	 * Sets the uploaded_timestamp.
	 *
	 * @param uploaded_timestamp the new uploaded_timestamp
	 */
	public void setUploaded_timestamp(String uploaded_timestamp) {
		this.uploaded_timestamp = uploaded_timestamp;
	}
	
	/**
	 * Gets the name_of_excelsheet.
	 *
	 * @return the name_of_excelsheet
	 */
	public String getName_of_excelsheet() {
		return name_of_excelsheet;
	}
	
	/**
	 * Sets the name_of_excelsheet.
	 *
	 * @param name_of_excelsheet the new name_of_excelsheet
	 */
	public void setName_of_excelsheet(String name_of_excelsheet) {
		this.name_of_excelsheet = name_of_excelsheet;
	}
	
	/**
	 * Gets the agent code.
	 *
	 * @return the agent code
	 */
	public String getAgentCode() {
		return employeeCode;
	}
	
	/**
	 * Sets the agent code.
	 *
	 * @param agentCode the new agent code
	 */
	public void setAgentCode(String agentCode) {
		this.employeeCode = agentCode;
	}
	
	/**
	 * Gets the employee id.
	 *
	 * @return the employee id
	 */
	public String getEmployeeId() {
		return employeeId;
	}
	
	/**
	 * Sets the employee id.
	 *
	 * @param employeeId the new employee id
	 */
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	
	/**
	 * Gets the fullname.
	 *
	 * @return the fullname
	 */
	public String getFullname() {
		return fullname;
	}
	
	/**
	 * Sets the fullname.
	 *
	 * @param fullname the new fullname
	 */
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	
	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public String getRole() {
		return role;
	}
	
	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(String role) {
		this.role = role;
	}
	
	/**
	 * Gets the adm code.
	 *
	 * @return the adm code
	 */
	public String getAdmCode() {
		return admCode;
	}
	
	/**
	 * Sets the adm code.
	 *
	 * @param admCode the new adm code
	 */
	public void setAdmCode(String admCode) {
		this.admCode = admCode;
	}
	
	/**
	 * Gets the brief write up.
	 *
	 * @return the brief write up
	 */
	public String getBriefWriteUp() {
		return briefWriteUp;
	}
	
	/**
	 * Sets the brief write up.
	 *
	 * @param briefWriteUp the new brief write up
	 */
	public void setBriefWriteUp(String briefWriteUp) {
		this.briefWriteUp = briefWriteUp;
	}
	
	/**
	 * Gets the trust factor.
	 *
	 * @return the trust factor
	 */
	public String getTrustFactor() {
		return trustFactor;
	}
	
	/**
	 * Sets the trust factor.
	 *
	 * @param trustFactor the new trust factor
	 */
	public void setTrustFactor(String trustFactor) {
		this.trustFactor = trustFactor;
	}
	
	/**
	 * Gets the advisor category.
	 *
	 * @return the advisor category
	 */
	public String getAdvisorCategory() {
		return advisorCategory;
	}
	
	/**
	 * Sets the advisor category.
	 *
	 * @param advisorCategory the new advisor category
	 */
	public void setAdvisorCategory(String advisorCategory) {
		this.advisorCategory = advisorCategory;
	}
	
	/**
	 * Gets the go.
	 *
	 * @return the go
	 */
	public String getGo() {
		return go;
	}
	
	/**
	 * Sets the go.
	 *
	 * @param go the new go
	 */
	public void setGo(String go) {
		this.go = go;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public  String toString()
	{
	    return "Agent Code:" + this.employeeCode+",, "+"Employe id:"
	                            +this.employeeId +",, "+"Full name:"
	                            +this.fullname+",, " +"Status:"
	                            +this.status+",, "+"Message:"
	                            +this.message;
	}

	/**
	 * Gets the employee type.
	 *
	 * @return the employee type
	 */
	public String getEmployeeType() {
		return employeeType;
	}

	/**
	 * Sets the employee type.
	 *
	 * @param employeeType the new employee type
	 */
	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}

	/**
	 * Gets the supervisor code.
	 *
	 * @return the supervisor code
	 */
	public String getSupervisorCode() {
		return supervisorCode;
	}

	/**
	 * Sets the supervisor code.
	 *
	 * @param supervisorCode the new supervisor code
	 */
	public void setSupervisorCode(String supervisorCode) {
		this.supervisorCode = supervisorCode;
	}

	/**
	 * Gets the office.
	 *
	 * @return the office
	 */
	public String getOffice() {
		return office;
	}

	/**
	 * Sets the office.
	 *
	 * @param office the new office
	 */
	public void setOffice(String office) {
		this.office = office;
	}

	/**
	 * Gets the group.
	 *
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * Sets the group.
	 *
	 * @param group the new group
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * Gets the years of experience.
	 *
	 * @return the years of experience
	 */
	public int getYearsOfExperience() {
		return yearsOfExperience;
	}

	/**
	 * Sets the years of experience.
	 *
	 * @param yearsOfExperience the new years of experience
	 */
	public void setYearsOfExperience(int yearsOfExperience) {
		this.yearsOfExperience = yearsOfExperience;
	}

	/**
	 * Gets the business sourced.
	 *
	 * @return the business sourced
	 */
	public Long getBusinessSourced() {
		return businessSourced;
	}

	/**
	 * Sets the business sourced.
	 *
	 * @param businessSourced the new business sourced
	 */
	public void setBusinessSourced(Long businessSourced) {
		this.businessSourced = businessSourced;
	}

	/**
	 * Gets the license number.
	 *
	 * @return the license number
	 */
	public String getLicenseNumber() {
		return licenseNumber;
	}

	/**
	 * Sets the license number.
	 *
	 * @param licenseNumber the new license number
	 */
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	/**
	 * Gets the license issue date.
	 *
	 * @return the license issue date
	 */
	public Date getLicenseIssueDate() {
		return licenseIssueDate;
	}

	/**
	 * Sets the license issue date.
	 *
	 * @param licenseIssueDate the new license issue date
	 */
	public void setLicenseIssueDate(Date licenseIssueDate) {
		this.licenseIssueDate = licenseIssueDate;
	}

	/**
	 * Gets the license expiry date.
	 *
	 * @return the license expiry date
	 */
	public Date getLicenseExpiryDate() {
		return licenseExpiryDate;
	}

	/**
	 * Sets the license expiry date.
	 *
	 * @param licenseExpiryDate the new license expiry date
	 */
	public void setLicenseExpiryDate(Date licenseExpiryDate) {
		this.licenseExpiryDate = licenseExpiryDate;
	}

	/**
	 * Gets the email id.
	 *
	 * @return the email id
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * Sets the email id.
	 *
	 * @param emailId the new email id
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * Gets the mobile number.
	 *
	 * @return the mobile number
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * Sets the mobile number.
	 *
	 * @param mobileNumber the new mobile number
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Gets the unit.
	 *
	 * @return the unit
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * Sets the unit.
	 *
	 * @param unit the new unit
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * Gets the num of cust serviced.
	 *
	 * @return the num of cust serviced
	 */
	public Long getNumOfCustServiced() {
		return numOfCustServiced;
	}

	/**
	 * Sets the num of cust serviced.
	 *
	 * @param numOfCustServiced the new num of cust serviced
	 */
	public void setNumOfCustServiced(Long numOfCustServiced) {
		this.numOfCustServiced = numOfCustServiced;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if(null!=obj && obj instanceof AgentDTO){
			return ((AgentDTO)obj).getAgentCode().equals(this.getAgentCode());
		}else{
			return false;
		}
		
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 17*13;
	}

	/**
	 * Gets the row number.
	 *
	 * @return the row number
	 */
	public int getRowNumber() {
		return rowNumber;
	}

	/**
	 * Sets the row number.
	 *
	 * @param rowNumber the new row number
	 */
	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	

}
