package com.cognizant.le.model.dao;

import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.cognizant.le.model.dto.Carrier;
import com.cognizant.le.model.dto.CodeLocaleLookup;
import com.cognizant.le.model.dto.CodeLookup;
import com.cognizant.le.model.dto.CodeRelation;
import com.cognizant.le.model.dto.CodeTypeLookup;
import com.cognizant.le.util.Constants;
import com.cognizant.le.util.ScriptRunner;

// TODO: Auto-generated Javadoc
/**
 * The Class LookupDAOImpl.
 */
@Repository
public class LookupDAOImpl implements LookupDAO {

	/** The jdbc template. */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** The environment. */
	@Autowired
	private Environment environment;
	
	/** The db type. */
	private String dbType;
	
	/** The schema. */
	private String schema;
	
	/**
	 * Insert carriers.
	 * 
	 * @param carrierList
	 *            the carrier list
	 */
	public void insertCarriers(final List<Carrier> carrierList) {
		String sql ="";
		if(dbType.equalsIgnoreCase("MSSQL")){
			sql = generateTableName(Constants.CARRIER_TBL);
		}
		sql+= environment.getProperty(Constants.INSERT_CARRIER);
		jdbcTemplate.batchUpdate(
				sql,
				new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i)
							throws SQLException {
						Carrier carrier = carrierList.get(i);
						ps.setInt(1, carrier.getCarrierId());
						ps.setString(2, carrier.getCarrierName());

					}

					public int getBatchSize() {
						return carrierList.size();
					}
				});
	}

	/**
	 * Insert types.
	 * 
	 * @param codeTypes
	 *            the code types
	 */
	public void insertTypes(final List<CodeTypeLookup> codeTypes) {
		String sql ="";
		if(dbType.equalsIgnoreCase("MSSQL")){
			sql = generateTableName(Constants.CODE_TYPE_TBL);
		}
		sql+=environment.getProperty(Constants.INSERT_CODE_TYPE_LOOKUP).replace(Constants.CODE_TYPE_TBL, schema+"."+Constants.CODE_TYPE_TBL);
		jdbcTemplate.batchUpdate(
				sql,
				new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i)
							throws SQLException {
						CodeTypeLookup codeTypeLookup = codeTypes.get(i);
						ps.setInt(1, codeTypeLookup.getId());
						ps.setString(2, codeTypeLookup.getTypeName());
						ps.setInt(3, codeTypeLookup.getCarrier_id());

					}

					public int getBatchSize() {
						return codeTypes.size();
					}
				});

	}

	/**
	 * Insert look up.
	 * 
	 * @param codeLookups
	 *            the code lookups
	 */
	public void insertLookUp(final List<CodeLookup> codeLookups) {
		String sql ="";
		if(dbType.equalsIgnoreCase("MSSQL")){
			sql = generateTableName(Constants.LOOKUP_TBL);
		}
		sql+=environment.getProperty(Constants.INSERT_CODE_LOOKUP).replace(Constants.LOOKUP_TBL, schema+"."+Constants.LOOKUP_TBL);
		jdbcTemplate.batchUpdate(
				sql,
				new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i)
							throws SQLException {
						CodeLookup codeLookup = codeLookups.get(i);
						ps.setInt(1, codeLookup.getId());
						ps.setString(2, codeLookup.getCode());
						ps.setString(3, codeLookup.getDescription());
						ps.setInt(4, codeLookup.getTypeId());
						ps.setBoolean(5, codeLookup.getIsDefault());

					}

					public int getBatchSize() {
						return codeLookups.size();
					}
				});

	}

	/**
	 * Insert relations.
	 * 
	 * @param codeRelations
	 *            the code relations
	 */
	public void insertRelations(final List<CodeRelation> codeRelations) {
		jdbcTemplate.batchUpdate(
				environment.getProperty(Constants.INSERT_RELATIONS).replace(Constants.RELATION_TBL, schema+"."+Constants.RELATION_TBL),
				new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i)
							throws SQLException {
						CodeRelation codeRelation = codeRelations.get(i);
						ps.setInt(1, codeRelation.getChildCode());
						ps.setInt(2, codeRelation.getParentCode());
						ps.setInt(3, codeRelation.getCodeType());
					}

					public int getBatchSize() {
						return codeRelations.size();
					}
				});

	}

	/**
	 * Insert locale.
	 * 
	 * @param codeLocaleLookups
	 *            the code locale lookups
	 */
	public void insertLocale(final List<CodeLocaleLookup> codeLocaleLookups) {
		jdbcTemplate.batchUpdate(
				environment.getProperty(Constants.INSERT_LOCALE_LOOKUP).replace(Constants.LOCALE_TBL, schema+"."+Constants.LOCALE_TBL),
				new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i)
							throws SQLException {
						CodeLocaleLookup codeLocaleLookup = codeLocaleLookups
								.get(i);
						ps.setString(1, codeLocaleLookup.getCountry());
						ps.setString(2, codeLocaleLookup.getLanguage());
						ps.setString(3, codeLocaleLookup.getValue());
						ps.setInt(4, codeLocaleLookup.getCodeId());
					}

					public int getBatchSize() {
						return codeLocaleLookups.size();
					}
				});

	}

	/**
	 * Delete existing entries. A temporary function to delete all the existing entries.
	 */
	public void deleteExistingEntries() {
		List<String> batchSqls = new ArrayList<String>();
		String sql[] = new String[10];
		if(dbType.equals("MYSQL")){
			batchSqls.add("DELETE FROM CODE_LOCALE_LOOKUP");
			batchSqls.add("DELETE FROM CODE_RELATION");
			batchSqls.add("DELETE FROM CODE_LOOKUP");
			batchSqls.add("DELETE FROM CODE_TYPE_LOOKUP");
		}else if(dbType.equalsIgnoreCase("MSSQL")){
			batchSqls.add("DELETE FROM "+schema+".CODE_LOCALE_LOOKUP");
			batchSqls.add("DELETE FROM "+schema+ ".CODE_RELATION");
			batchSqls.add("DELETE FROM "+schema+ ".CODE_LOOKUP");
			batchSqls.add("DELETE FROM "+schema+ ".CODE_TYPE_LOOKUP");
		}
		sql = Arrays.copyOf(batchSqls.toArray(), batchSqls.toArray().length, String[].class);
		jdbcTemplate.batchUpdate(sql);
	}
	
	/**
	 * Check table.
	 *
	 * @param schemaName the schema name
	 * @param tableName the table name
	 * @return the boolean
	 * @throws DataAccessException the data access exception
	 */
	public Boolean checkTable(String schemaName, String tableName) throws DataAccessException{
		String query = environment.getProperty(Constants.CHECK_TABLE);
		query = query.replace("table_schema", schemaName);
		query = query.replace("table_name", tableName);
		Integer count =  jdbcTemplate.queryForInt(query);
		if(count!=1){
			return false;
		}else{
			return true;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.cognizant.le.model.dao.LookupDAO#createTable(java.io.InputStreamReader)
	 */
	public void createTable(InputStreamReader isr){
		Connection con =null;
		try {
			con = jdbcTemplate.getDataSource().getConnection();
			ScriptRunner scr = new ScriptRunner(con,false,true);
			scr.runScript(isr);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	
	
	/* (non-Javadoc)
	 * @see com.cognizant.le.model.dao.LookupDAO#turnOnIdentityInsert(java.lang.String)
	 */
	public void turnOnIdentityInsert(String tableName) throws SQLException{
		String sql = "SET IDENTITY_INSERT "+ tableName+ " ON";
		Connection con = null;
		try{
			con = jdbcTemplate.getDataSource().getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			ps.executeUpdate();
			ps.close();
		}catch(SQLException sq){
			throw sq;
		}finally{
			if(con!=null){
				try{
				con.close();
				}catch (SQLException e) {
					throw e;
				}
			}
		}
		
	}
	
	/**
	 * Turn on identity insert.
	 *
	 * @param schema the schema
	 * @param table the table
	 * @throws SQLException the sQL exception
	 */
	public String generateTableName(String table){
		table = schema+"."+table;
		String sql = "SET IDENTITY_INSERT "+ table+ " ON;";
		return sql;
	}

	/**
	 * Gets the db type.
	 *
	 * @return the db type
	 */
	public String getDbType() {
		return dbType;
	}

	/**
	 * Sets the db type.
	 *
	 * @param dbType the new db type
	 */
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}

	/**
	 * Gets the schema.
	 *
	 * @return the schema
	 */
	public String getSchema() {
		return schema;
	}

	/**
	 * Sets the schema.
	 *
	 * @param schema the new schema
	 */
	public void setSchema(String schema) {
		this.schema = schema;
	}

	@Autowired
	public LookupDAOImpl(Environment environment) {
		super();
		this.dbType = environment.getProperty(Constants.DBTYPE);
		this.schema = environment.getProperty(Constants.SCHEMA);
		if(schema == null){
			schema ="dbo";
		}
	}


}
