	package com.cognizant.le.action;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.cognizant.le.model.dao.ContentAdminDAO;
import com.cognizant.le.model.dao.ContentAdminDAOImpl;
import com.cognizant.le.model.dto.AppContext;
import com.cognizant.le.model.dto.ContentFileDTO;
import com.cognizant.le.model.dto.ContentLanguageDTO;
import com.cognizant.le.model.dto.ContentTypeDTO;

/**
 * Servlet to handle File upload request from Client
 * 
 * @author 304003
 */

@WebServlet("/ContentUploadServlet")
public class ContentUploadServlet extends HttpServlet {
	private static Logger LOGGER = Logger.getLogger(ContentUploadServlet.class);
	private static final long serialVersionUID = 4470044101132876855L;
	private String contentFolder="";
	private String serverURL="";

	@Override
	public void init(ServletConfig config) throws ServletException {
		LOGGER.debug("Method Entry : ContentUploadServlet.init");
		super.init(config);
		String propertiesFile = getServletContext().getInitParameter("CONFIG_PROPERTIES");
		Properties properties = new Properties();
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream stream = classLoader.getResourceAsStream("/" + propertiesFile);
			if(null == stream) {
				stream = classLoader.getResourceAsStream(propertiesFile);
			}
			properties.load(stream);			
			contentFolder = properties.getProperty("SHARED_CONTENT_FOLDER");
			if((null==contentFolder) || "".equals(contentFolder.trim())) {
				LOGGER.error("Property SHARED_CONTENT_FOLDER is not defined");
			} 
			LOGGER.debug("Upload directory for contents is " + contentFolder);
			serverURL = properties.getProperty("SERVER_URL");
			if((null==serverURL) || "".equals(serverURL.trim())) {
				LOGGER.error("Property SERVER_URL is not defined");
			} 
			LOGGER.debug("ServerURL is " + serverURL);
		} catch (IOException e) {
			LOGGER.error(propertiesFile + " is missing", e);
			throw new ServletException(e.getMessage(), e);
		}
		LOGGER.debug("Method Exit : ContentUploadServlet.init");
	}
	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		LOGGER.debug("Method Entry : ContentUploadServlet.doPost");
		HttpSession session = request.getSession(true);
		String fileName = null;
		String contentType = "";
		String language = null;
		String isRequired = null;
		String comment= request.getParameter("update_comment_textarea");
		List<Integer> contexts = new ArrayList<Integer>();
		List<AppContext> selectedAppContexts= new ArrayList<AppContext>();
		Long fileSize = null;
		String fileDesc = null;
		// process only if its multipart content
		if (ServletFileUpload.isMultipartContent(request)) {
			try {
				List<FileItem> multiparts = new ServletFileUpload(
						new DiskFileItemFactory()).parseRequest(request);
				for (FileItem item : multiparts) {
					if (item.isFormField()) {
						if ("Filter".equalsIgnoreCase(item.getFieldName())) {
							contentType = item.getString();
						}
						else if ("Language".equalsIgnoreCase(item.getFieldName())) {
							language = item.getString();
						}
						else if ("doc_req".equalsIgnoreCase(item.getFieldName())) {
							isRequired = item.getString();
						}
						else if("context_name".equalsIgnoreCase(item.getFieldName())){
							System.out.println(item.getString());
							AppContext appContext= new AppContext();
							contexts.add(Integer.parseInt(item.getString()));
							appContext.setId(Integer.parseInt(item.getString()));
							selectedAppContexts.add(appContext);
						}
						else if ("desc_text".equalsIgnoreCase(item.getFieldName())) {
							fileDesc = item.getString();
						}
					}
					if (!item.isFormField()) {
					fileName = new File(item.getName()).getName();
					fileSize = item.getSize();
					    if(fileName.lastIndexOf("/") != -1) {
				            fileName = fileName.substring(fileName.lastIndexOf("/") + 1,fileName.length());
				        } else if (fileName.lastIndexOf("\\") != -1) {
				            fileName = fileName.substring(fileName.lastIndexOf("\\") + 1,fileName.length());
				        }
/*					    File uploadDir=new File(contentFolder + File.separator+"1");
						if(!uploadDir.exists()){
							uploadDir.mkdir();
						}*/
						item.write(new File(contentFolder,fileName));
					}
				}

				// File uploaded successfully

				// insert file name and content type to db
				if(null==isRequired){
					isRequired="false";
				}
				
				JSONObject sessionObj = (JSONObject)session.getAttribute("contentPreference");
				JSONObject contPrefObj = (JSONObject)sessionObj.get("contentPreference");
				Boolean optionality =(Boolean)((JSONObject)contPrefObj.get(new Integer(contentType))).get("optionality");
				if(optionality==false){
					isRequired="true";
				}
				
				ContentFileDTO contentFileDTO = new ContentFileDTO();
				
				ContentTypeDTO contentTypeDTO = new ContentTypeDTO();
				if(null==contentType){
					throw new IOException("Error in content type.");
				}
				contentTypeDTO.setId(Integer.valueOf(contentType));
				
				ContentLanguageDTO contentLanguageDTO = null;
				if(null!=language && language.length()>0){
				contentLanguageDTO = new ContentLanguageDTO();
				contentLanguageDTO.setId(Integer.parseInt(language));
				}
				
				contentFileDTO.setContentType(contentTypeDTO);
				contentFileDTO.setContentLanguage(contentLanguageDTO);
				contentFileDTO.setFileName(fileName);
				contentFileDTO.setVersion(1);
				contentFileDTO.setLastUpdated(new Date(System
						.currentTimeMillis()));
				contentFileDTO.setPublicURL(getDownloadURL(fileName,request));
				contentFileDTO.setComments(comment);
				contentFileDTO.setAppContexts(selectedAppContexts);
				contentFileDTO.setIsRequired(isRequired.equals("true")?true:false);
				contentFileDTO.setFileSize(fileSize);
				contentFileDTO.setFileDesc(fileDesc);
				ContentAdminDAO contentAdminServiceDAO = new ContentAdminDAOImpl();
				try {
					if (contentAdminServiceDAO.isContentExists(contentFileDTO)) {
						session.setAttribute("uploadMsg", "Content Already Exists");
						
					} else {
						contentAdminServiceDAO.insertContent(contentFileDTO);
						session.setAttribute("uploadMsg",
								"Content Uploaded Successfully");
						
					}
				} catch (Exception e) {
					LOGGER.error(e);
					throw new ServletException(e);
				}
			} catch (Exception ex) {
				LOGGER.error("Content Upload Failed!!! ", ex);
				request.setAttribute("filename", fileName);
				request.setAttribute("comments", comment!=null?comment:"");
				request.setAttribute("selected-contexts", contexts);
				request.setAttribute("content-type", contentType);
				//request.setAttribute("appContexts", new ContentAdminServiceDAOImpl().getApplicationContexts());
				session.setAttribute("uploadMsg", "Content Upload Failed due to "
						+ ex.getMessage());
			}

		} else {
		    session.setAttribute("uploadMsg",
					"Sorry this Servlet only handles file upload request");
		}
		request.setAttribute("filename", fileName);
		request.setAttribute("comments", comment!=null?comment:"");
		request.setAttribute("selected-contexts", contexts);
		request.setAttribute("content-type", contentType);
		//request.setAttribute("appContexts", new ContentAdminServiceDAOImpl().getApplicationContexts());
		request.getRequestDispatcher("jsp/ContentList.jsp").forward(request, response);
		LOGGER.debug("Method Exit : ContentUploadServlet.doPost");
	}

	
	private String getDownloadURL(String fileName, HttpServletRequest request) {
		LOGGER.debug("Method Entry : ContentUploadServlet.getDownloadURL");
		StringBuilder downloadURL = new StringBuilder("/downloadContent?fileName=");
		downloadURL.append(fileName);
		LOGGER.debug("Download URL Formed ---" + downloadURL);
		LOGGER.debug("Method Exit : ContentUploadServlet.getDownloadURL");
		return downloadURL.toString();
	}
	
	
}
