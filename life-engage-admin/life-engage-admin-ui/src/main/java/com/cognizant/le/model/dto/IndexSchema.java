/**
 *
 *  Copyright 2012, Cognizant
 *
 * @author        : 287304
 * @version       : 0.1, May 16, 2014
 */
package com.cognizant.le.model.dto;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class IndexSchema.
 */
public class IndexSchema
{
    
    /** The index name. */
    public String indexName;

    /** The is unique. */
    public Boolean isUnique;

    /** The columns. */
    public List<IndexColumn> columns;

	/**
	 * Gets the index name.
	 *
	 * @return the index name
	 */
	public String getIndexName() {
		return indexName;
	}

	/**
	 * Sets the index name.
	 *
	 * @param indexName the new index name
	 */
	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	/**
	 * Gets the checks if is unique.
	 *
	 * @return the checks if is unique
	 */
	public Boolean getIsUnique() {
		return isUnique;
	}

	/**
	 * Sets the checks if is unique.
	 *
	 * @param isUnique the new checks if is unique
	 */
	public void setIsUnique(Boolean isUnique) {
		this.isUnique = isUnique;
	}

	/**
	 * Gets the columns.
	 *
	 * @return the columns
	 */
	public List<IndexColumn> getColumns() {
		return columns;
	}

	/**
	 * Sets the columns.
	 *
	 * @param columns the new columns
	 */
	public void setColumns(List<IndexColumn> columns) {
		this.columns = columns;
	}
}