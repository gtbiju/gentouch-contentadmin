  CREATE TABLE [~~~].[CODE_RELATION] (
  [id] bigint NOT NULL IDENTITY,
  [basicDataCompleteCode] int DEFAULT NULL,
  [contextId] int DEFAULT NULL,
  [creationDateTime] date DEFAULT NULL,
  [transactionId] varchar(255) DEFAULT NULL,
  [typeName] varchar(255) DEFAULT NULL,
  [child_id] bigint DEFAULT NULL FOREIGN KEY REFERENCES [~~~].[CODE_LOOKUP] ([id]),
  [parent_id] bigint DEFAULT NULL FOREIGN KEY REFERENCES [~~~].[CODE_LOOKUP] ([id]),
  [type_id] bigint DEFAULT NULL FOREIGN KEY REFERENCES [~~~].[CODE_TYPE_LOOKUP] ([id]),
  PRIMARY KEY ([id])
);
  