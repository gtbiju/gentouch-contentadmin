package com.cognizant.le.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cognizant.le.model.dao.ContentAdminDAO;
import com.cognizant.le.model.dao.ContentAdminDAOImpl;
import com.cognizant.le.model.dto.AppContext;
import com.cognizant.le.model.dto.ContentFileDTO;
import com.cognizant.le.model.dto.ContentLanguageDTO;

/**
 * Servlet to handle tagUntag Context request from client
 * 
 */
@WebServlet("/ContextTagUntagServlet")
public class ContextTagUntagServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static Logger LOGGER = Logger.getLogger(ContextTagUntagServlet.class);
	private String contentFolder = ""; 
   
    @Override
	public void init(ServletConfig config) throws ServletException {
		LOGGER.debug("Method Entry : ContextTagUntagServlet.init");
		super.init(config);
		String propertiesFile = getServletContext().getInitParameter(
				"CONFIG_PROPERTIES");
		Properties properties = new Properties();
		try {
			ClassLoader classLoader = Thread.currentThread()
					.getContextClassLoader();
			InputStream stream = classLoader.getResourceAsStream("/"
					+ propertiesFile);
			if (null == stream) {
				stream = classLoader.getResourceAsStream(propertiesFile);
			}
			properties.load(stream);
			contentFolder = properties.getProperty("SHARED_CONTENT_FOLDER");
			LOGGER.debug("Upload directory for contents is " + contentFolder);
			LOGGER.debug("Method Exit : ContextTagUntagServlet.init");
		} catch (IOException e) {
			LOGGER.error(propertiesFile + " is missing", e);
			throw new ServletException(e.getMessage(), e);
		}
	}

	
    /**
     * Method call on clicking Tag-Untag button
     */
    @Override
    protected void doPost(HttpServletRequest request,
    		HttpServletResponse response) throws ServletException, IOException {

    	LOGGER.debug("Method Entry : ContextTagUntagServlet.doPost");
    	HttpSession session = request.getSession();
    	List<AppContext> selectedAppContexts= new ArrayList<AppContext>();

    	String fileName = request.getParameter("tagUntagContentFileName");
    	String language = request.getParameter("Language");
    	String checkedAppContext = request.getParameter("selectedAppContext");
    	String isRequired = request.getParameter("doc_req_udt") ;
    	String isDisabled = request.getParameter("is_disabled") ;
    	if(isRequired==null && null!=isDisabled && isDisabled.equalsIgnoreCase("true")){
    		isRequired = request.getParameter("doc_opt_udt_val") ;
    	}
    	if(language==null && null!=isDisabled && isDisabled.equalsIgnoreCase("true")){
    		language = request.getParameter("edt_slctd_lng") ;
    	}
    	if(null==isRequired){
    		isRequired = "false";
    	}
    	String selectedContentType = request.getParameter("tagUntagSelectedType");
    	try {

    		LOGGER.debug(request.getParameter("tagUntagContentFileName"));

    		ContentFileDTO contentFileDTO= new ContentFileDTO();
    		contentFileDTO.setFileName(fileName);

    		if( checkedAppContext != null && checkedAppContext.length() > 0){
    			String[] selectedAppContext = checkedAppContext.split(",");
    			for(String appContextChkd: selectedAppContext){
    				AppContext appContext= new AppContext();
    				appContext.setId(Integer.parseInt(appContextChkd));
    				selectedAppContexts.add(appContext);
    			}
    		}
    		contentFileDTO.setAppContexts(selectedAppContexts);
    		
    		ContentLanguageDTO contentLanguageDTO = new ContentLanguageDTO();
    		contentLanguageDTO.setId(Integer.parseInt(language));
    		
    		contentFileDTO.setContentLanguage(contentLanguageDTO);
    		if(isRequired!=null && isRequired.length()>0){
    		contentFileDTO.setIsRequired(isRequired.equals("true")?true:false);
    		}
    		ContentAdminDAO contentAdminServiceDAO = new ContentAdminDAOImpl();
    		contentAdminServiceDAO.tagUntagContext(contentFileDTO);

    		request.setAttribute("contentFileName", contentFileDTO.getFileName());
    		request.setAttribute("language", language);
    		request.setAttribute("selectedAppContext", checkedAppContext);
    		request.setAttribute("isMandatory", isRequired);
    		request.setAttribute("selectedContentType", selectedContentType);
    		request.setAttribute("tagUntagMsg", "Content edited successfully");
    		//request.setAttribute("appContexts", new ContentAdminServiceDAOImpl().getApplicationContexts());
    		
    	} catch (Exception e) {
    		LOGGER.error("Context Tag/Un-tag Failed!!!", e);
    		request.setAttribute("contentFileName", fileName);
    		request.setAttribute("language", language);
    		request.setAttribute("selectedAppContext", checkedAppContext);
    		request.setAttribute("isMandatory", isRequired);
    		request.setAttribute("selectedContentType", selectedContentType);
    		request.setAttribute("tagUntagMsg","Content edit failed due to " + e.getMessage());
    		//request.setAttribute("appContexts", new ContentAdminServiceDAOImpl().getApplicationContexts());
    	}
    	finally{
    		request.getRequestDispatcher("/jsp/ContentList.jsp").forward(request, response);
    	}
    }

}
