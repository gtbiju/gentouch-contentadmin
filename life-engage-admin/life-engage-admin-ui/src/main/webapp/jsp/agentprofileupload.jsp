<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<%@ page import="com.cognizant.le.model.dto.AgentDTO" %>
<%@ page import="com.cognizant.le.model.dto.AgentProfile" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<link rel="stylesheet" href="css/css_reset.css" />
<link rel="stylesheet" href="css/styles_workbench.css" />
<title>DAP</title>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<style>
.contentPosition {
width: 30%;
background-color: white;
height: 165px;
margin: auto;
text-align: center;
}

.agent_custom_upload {
margin: 20px;
width: 100%;
float:left;
left: 50%;
}

.upload_agent_button {
width:75%;
}

.mand_statement_ap {
font-size: 11px;
padding-left: 7px;
float: left;
}

#agent_upload_pop_up_btn {
float: right;
margin-right: 59px;
}
</style>
<script>
var SITE = SITE || {};

SITE.fileInputs = function() {
  var $this = $(this),
      $val = $this.val(),
      valArray = $val.split('\\'),
      newVal = valArray[valArray.length-1],
      $button = $this.siblings('.button'),
      $fakeFile = $this.siblings('.file-holder');
  if(newVal !== '') {
    $button.text('Browse');
    if($fakeFile.length === 0) {
      $button.after('<span class="file-holder">' + newVal + '</span>');
    } else {
      $fakeFile.text(newVal);
    }
  }
};

$(document).ready(function() {
	document.getElementById("display_message").innerHtml="";
	$('.custom-upload input[type=file]').change(function(){
	    $(this).next().find('input').val($(this).val());
	});
	
	<%
	if(null== request.getSession(false)){%>
		logoutClick();
	<%	
	}
	%> 		

  $('.custom-upload input[type=file]').bind('change focus click', SITE.fileInputs);
  var currentDate = new Date();
  $('#spanId').append(moment().format('h:mm A Do MMMM YYYY'));
  
  var profile= "<%=request.getAttribute("profile")%>"
  if(profile.length > 4){
			    $('#page_loader').empty();
		var dynamic_height = $('body').height();
		$('#page_loader').css('height',dynamic_height).fadeIn(300);
	  agntUploadResults();
  }
  
	$('.close_pop_up').click(function(){	 
	   $('.modal_pop_up.agtupd_content_pop_up').css("display","none");
	  $("body").removeClass("hide_scroll"); 
	  $('#page_loader').css("display","none");
	});
	//disabling upload button
	$('input:file').change(
			
            function(){
				document.getElementById("display_message").innerHTML="";
                if ($(this).val()) {
                    $('#agent_upload_pop_up_btn').attr('disabled',false);
                    // or, as has been pointed out elsewhere:
                    // $('input:submit').removeAttr('disabled'); 
                } 
            });
			
	$('#agent_upload_pop_up_btn').click(function(e){
		var fileName = $("#fileName").val();  
	  var extension = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
	  if(extension == "xls" || extension == "xlsx" ){
		  document.uploadForm.submit();  
	  } else if(extension == "") {
		document.getElementById("display_message").innerHTML="Please select a file...";
		e.preventDefault(); 
	  }
	  else{
	  	document.getElementById("display_message").innerHTML="Invalid file type !!";
		e.preventDefault(); 
	  }
	});	
			
	
});
</script>
    
  <script type="text/javascript">
  function logoutClick(){
		document.location.href="<%=request.getContextPath()%>/Logout";
	}
  function showPopUp(){
	  $(".pageContainer").hide();
	  $("#popupDiv").show();
  }
  
  function closePopup(){
	  $("#popupDiv").hide();
	  $(".pageContainer").show();	
	  $("#fileName").val('');
	  
  }
  
  $('#closeDiv').click( function() {            
	  $("#popupDiv").hide();
	  $(".pageContainer").show();
	  $("#fileName").val('');
  });
  
	function agentPage(){
		document.location.href="<%=request.getContextPath()%>/AgentUpload";
	}
	
	function contentPage(){
		document.location.href="<%=request.getContextPath()%>/loadFile";
	}
	function mdbLoader(){
		document.location.href="<%=request.getContextPath()%>/jsp/masterDataTemplate.jsp";
	}
	
  </script>
</head>
<body>
	<div class="blue_bg_header">
		<div class="top_header_components clearfix">
			<span class="environment_txt">Environment</span> <select
				id="filter_environment">
				<option value="Model Office">Model Office</option>
				<option value="Production">Production</option>
				<option value="UAT">UAT</option>
				<option value="Dev">Dev</option>
			</select>
			<button class="logout_action_btn" id="logout_btn" onClick="logoutClick()">Logout</button>
		</div>
	</div>
		<div class="logo_header">
	<img src="img/logo-main.png" alt="logo-main" class="mainLogo" > <span
			class="header_title">LifeEngage Configurator</span>
	</div>
		<div class="containerMain">
		<div class="containerMenu ">
			<div class="TopSlideMenu main_nav_bar nav_bg_green">
				<ul class="main_nav_bar_list clearfix">
					<!-- <li class="activeList"><a href="javascript:void(0)" rel="#showTop">About us</a></li>
					<li class="activeList"><a href="javascript:void(0)" rel="#showTop1">Services</a></li>
					<li class="activeList"><a href="javascript:void(0)" rel="#showTop2">Products</a></li> -->

					<li class="active activeList last_li"><a class="clearfix"
						 rel="#showUsers" onclick=""><span
							class="menu_link_text">Users</span></a></li>
					<li class="activeList"><a class="clearfix"
						onclick="contentPage()" rel="#showContent"><span
							class="menu_link_text">Content</span></a></li>
					<li class="activeList last_li"><a class="clearfix"
						onClick="mdbLoader();" rel="#showContent"><span
							class="menu_link_text">Master Data</span></a></li>		
				</ul>
			</div>
			<div class="submenu_container slide-menu-top" id="showProducts">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li class="activated"><a href="#" title="menu">Application
									innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>
							<li><a href="#" title="menu">Business strategy</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>

			<div class="submenu_container slide-menu-top" id="showUsers">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>
							<li><a href="#" title="menu">Business strategy</a></li>

						</ul>
					</div>
				</div>
				<div class="submenu_column2">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>
							<li><a href="#" title="menu">Business strategy</a></li>

						</ul>
					</div>
				</div>
				<div class="clear"></div>
			</div>

			<div class="submenu_container slide-menu-top" id="showContent">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li class="activated"><a href="#" title="menu">
									Add/Update Content </a></li>
						</ul>
					</div>
				</div>

				<div class="clear"></div>
			</div>
			<div class="submenu_container slide-menu-top" id="showApp">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>
			<div class="submenu_container slide-menu-top" id="showRules">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>
			<div class="submenu_container slide-menu-top" id="showCalcs">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>
		</div>
		<div class="clear"></div>
		</div>
		
			<div class="bg_white">
		<ul class="breadcrumb breadcrumb_gradientbg">
			<li><a href="#" class="brd_link_sel">Users</a></li>
		</ul>
	</div>
	
		<!-- CONTENT CONTAINER -->
	<div class="content_container">
		<div class="action_btn_container clearfix">
		</div>
		<div class="container_tlr_fluid">
			<div class="blankContainer">
				<div class="blankContainerInner">
					<div class="table_container">
							    <form action="upload" name="uploadForm" method="post" enctype="multipart/form-data">
									<div class="pageContainer">
										<div class="contentPosition">
													<p class="modal_pop_up_header ">
													Browse a file to upload
													</p>
											
													
													<div class="agent_custom_upload">
													<span style="text-align:left;"><h3 style="margin-bottom: 5px;">Select the excel sheet with the agent details.</h3></span>
														<div class="custom-upload">
															
															
															<input type="file" id="fileName" name="fileName">
															<div class="upload_cont">
																<input disabled="disabled">
																<button class="browse_pop_up_btn" id="browse_btn" >
																Browse</button>
															</div>
														</div>
													</div>
													
														<div class="upload_agent_button">
															<button class="pop_up_upload_btn mrg_update" id="agent_upload_pop_up_btn">Upload</button>
														</div>
													<span id="display_message" class="clearfix upload_message"></span>

										</div>
									</div>
								</form>
					</div>

				</div>
			</div>
		</div>		
		
	</div>	
	
		<!-- CONTENT CONTAINER -->



	<div class="footer_container">
		<span class="copyright_label">&copy;Copyright 2013. LifeEngage
		</span>
	</div>


	<div id="page_loader"></div>
	
    
   <div id="popupDiv" class="popup popup2"  style="display:none;">
        <div class="popupHeading">
            <a onclick="closePopup()">close</a>
        </div>
        <div class="popupContent">
        	Invalid file name
        </div>
        <div class="shadow"></div>
        <div class="btn">
        	 <span>
                <input type="button" id="closeDiv" class="orangeButton" value="OK" onclick="closePopup()"/>            
            </span>
		</div>
    </div> 
    
    	<!-- THE MODAL AGENTUPLOADRESULT CONTENT POPUP -->
	<div class="modal_pop_up agtupd_content_pop_up" style="height:75%; overflow: auto; width:50%">
		<p class="modal_pop_up_header">
			Agent Profile Upload Results <span class="close_pop_up"></span>
		</p>
	    <div class="blankContainerInner">
            <%
            if(null!=request.getAttribute("profile")){
            AgentProfile profile=(AgentProfile)request.getAttribute("profile");
            boolean validAgent=profile.isAgentValid();
            boolean validAchv=profile.isAchvValid();
            if ((!validAgent)&& (!validAchv)){
            %>
            <table  border="0" cellpadding="0" cellspacing="0" class="cust_tbl">
              <tr class="tbl_head_bg">
                <th>Agent Code</th>
                <th>Agent Name</th>
                <th>User ID</th>
                <th>Status </th>
                <th>Message</th>
              </tr>
              
            <%
                        
            List<AgentDTO> agents=(List<AgentDTO>)profile.getAgents();
            if(null!=agents){
            for (AgentDTO agent : agents) { 
			%>
			<tr style="background-color:#f0f0f0">
			<%if(agent.getAgentCode()!=null){ %>
  			<td><%=agent.getAgentCode() %></td>
  			<%} else{%>
  			<td><div style="word-wrap: break-word; "><%="" %></div></td>
  			<%} %>
  			<%if(agent.getFullname()!=null){ %>
  			<td><div style="word-wrap: break-word; "><%=agent.getFullname() %></div></td>
  			<%} else{%>
  			<td><div style="word-wrap: break-word; "><%="" %></div></td>
  			<%} %>
  			<%if(agent.getUserid()!=null){ %>
  			<td ><div style="word-wrap: break-word; "><%=agent.getUserid() %></div></td>
  			<%}else{ %>
  			<td ><div style="word-wrap: break-word;"><%="" %></div></td>
  			<%} %>
  			<%if(agent.getStatus()!=null){ %>
  			<td><%=agent.getStatus() %></td>
  			<%} else{%>
  			<td><div style="word-wrap: break-word; "><%="" %></div></td>
  			<%} %>
  			<%if(agent.getMessage()!=null){ %>
 		 	<td><%=agent.getMessage()%></td>
 		 	<%} else{%>
  			<td><div style="word-wrap: break-word; "><%="" %></div></td>
  			<%} %>
			</tr> <% }} %>
			<tr><td colspan="5"><b> Agents Total: <%=profile.getTotalAgents()%> Success : <%=profile.getSuccessAgents()%> Fail: <%=profile.getFailAgents()%> Invalid User :<%=profile.getInvalidAgent()%> </b><br>
			</td></tr>
            </table>

        <% } if(null!=profile.getMessages()&&profile.getMessages().size()>0) { int i=0; %> 
        <h4>Please fix to continue.</h4>
        <% for(String message : profile.getMessages()){  %>
      <font color='red'> <b><%= ++i %> : <%=message %></b></font><br> 
        
       <% }}} %> 

   </div>	

	</div>
	<!-- THE MODAL AGENTUPLOADRESULT CONTENT POPUP -->
    
    
    
</body>
</html>
