/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 298738
 * @version       : 0.1, Apr 11, 2013
 */
package com.cognizant.le.mdu.core.config;

import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;
import org.jasypt.spring31.properties.EncryptablePropertyPlaceholderConfigurer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.cognizant.le.util.Constants;
import com.mchange.v2.c3p0.ComboPooledDataSource;

// TODO: Auto-generated Javadoc
/**
 * The Class class WebConfig.
 */
@Configuration
@EnableWebMvc
@PropertySource({ "classpath:/"+Constants.PROPERTY_FILE})
@ComponentScan(basePackages = { "com.cognizant.le" })
@EnableTransactionManagement
public class WebConfig extends WebMvcConfigurerAdapter {

	/** The driver class. */
	private @Value("${le.mdu.app.jdbc.driverClassName}") String driverClass;
	
	/** The jdbc url. */
	private @Value("${le.mdu.app.jdbc.url}") String jdbcURL;
	
	/** The user name. */
	private @Value("${le.mdu.app.jdbc.username}") String userName;
	
	/** The password. */
	private @Value("${le.mdu.app.jdbc.password}") String password;



    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
     * #addViewControllers(org.springframework.web.servlet.config.annotation. ViewControllerRegistry)
     */
    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/index.html").setViewName("index");
    }

    /**
     * View resolver.
     * 
     * @return the internal resource view resolver
     */
    @Bean
    public InternalResourceViewResolver viewResolver() {
        final InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/jsp/");
        resolver.setSuffix(".jsp");
        resolver.setOrder(1);
        return resolver;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
     * #addResourceHandlers(org.springframework.web.servlet.config.annotation. ResourceHandlerRegistry)
     */
    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
        registry.addResourceHandler("/css/**").addResourceLocations("/css/");
        registry.addResourceHandler("/img/**").addResourceLocations("/img/");
        registry.addResourceHandler("/js/**").addResourceLocations("/js/");
        registry.addResourceHandler("/fonts/**").addResourceLocations("/fonts/");
        
        registry.addResourceHandler("/lib/**").addResourceLocations("/resources/lib/");
        registry.addResourceHandler("/controller/**").addResourceLocations("/resources/controller/");
        registry.addResourceHandler("/directive/**").addResourceLocations("/resources/directive/");
        registry.addResourceHandler("/template/**").addResourceLocations("/resources/template/");
        registry.addResourceHandler("/jsp/css/**").addResourceLocations("/resources/css/");
        registry.addResourceHandler("/jsp/images/**").addResourceLocations("/resources/images/");
        registry.addResourceHandler("/jsp/lib/**").addResourceLocations("/resources/lib/");
        registry.addResourceHandler("/jsp/controller/**").addResourceLocations("/resources/controller/");
        registry.addResourceHandler("/jsp/directive/**").addResourceLocations("/resources/directive/");
        registry.addResourceHandler("/jsp/template/**").addResourceLocations("/resources/template/");
    }
    
    /**
     * Data source.
     * 
     * @return the data source
     * @throws PropertyVetoException
     *             the exception
     */
    @Bean(destroyMethod = "close")
    public DataSource dataSource() throws PropertyVetoException {

        final ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setDriverClass(driverClass);
        dataSource.setJdbcUrl(jdbcURL);
        dataSource.setUser(userName);
        dataSource.setPassword(password);
        dataSource.setAcquireIncrement(5);
        dataSource.setIdleConnectionTestPeriod(60);
        dataSource.setMaxPoolSize(100);
        dataSource.setMaxStatements(50);
        dataSource.setMinPoolSize(10);
        return dataSource;
    }
    
    /**
     * Jdbc template.
     * 
     * @return the jdbc template
     * @throws PropertyVetoException
     *             the property veto exception
     */
    @Bean
    public JdbcTemplate jdbcTemplate() throws PropertyVetoException {
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource());
        return jdbcTemplate;
    }

    
	/**
	 * Multipart resolver.
	 *
	 * @return the multipart resolver
	 */
	@Bean
	public MultipartResolver multipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setMaxUploadSize(5 * 1024 * 1024);
		return multipartResolver;
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#configureMessageConverters(java.util.List)
	 */
	@Override
		public void configureMessageConverters(
				List<HttpMessageConverter<?>> converters) {
		converters.add(converter());
		}
	
    /**
     * Converter.
     *
     * @return the mapping jackson http message converter
     */
    @Bean
    MappingJacksonHttpMessageConverter converter() {
        MappingJacksonHttpMessageConverter converter = new MappingJacksonHttpMessageConverter();
        List<MediaType> supportedMediaTypes = new ArrayList<MediaType>();
        supportedMediaTypes.add(MediaType.TEXT_PLAIN);
        supportedMediaTypes.add(MediaType.APPLICATION_JSON);
		converter.setSupportedMediaTypes(supportedMediaTypes);
        return converter;
    }
    
    /**
	 * Property configurer. PropertyPlaceHolder must be explicitly registered using a static @Bean method when using @Configuration classes
	 *
	 * @return the encryptable property placeholder configurer
	 */
	@Bean
	public static EncryptablePropertyPlaceholderConfigurer propertyConfigurer(){
		EncryptablePropertyPlaceholderConfigurer encCnfg = new EncryptablePropertyPlaceholderConfigurer(stringEncryptor());
		Resource location = new ClassPathResource(Constants.PROPERTY_FILE);
		encCnfg.setLocation(location);
		return encCnfg;
	}
	
	/**
	 * String encryptor.
	 *
	 * @return the standard pbe string encryptor
	 */
	@Bean
	public static StandardPBEStringEncryptor stringEncryptor(){
		StandardPBEStringEncryptor pbeEncryptor = new  StandardPBEStringEncryptor();
		pbeEncryptor.setPassword(Constants.ENC_VAR+"."+Constants.ENC_VAR_PART);
		return pbeEncryptor;
	}
	
	/**
	 * Env config.
	 *
	 * @return the environment string pbe config
	 */
	@Bean
	public static EnvironmentStringPBEConfig envConfig(){
		EnvironmentStringPBEConfig envConfig = new EnvironmentStringPBEConfig();
		envConfig.setAlgorithm(Constants.ENC_ALGM);
		//envConfig.setPasswordEnvName(Constants.ENC_VAR);
		envConfig.setPassword(Constants.ENC_VAR+"."+Constants.ENC_VAR_PART);
		return envConfig;
	}

	/**
	 * Gets the driver class.
	 *
	 * @return the driver class
	 */
	public String getDriverClass() {
		return driverClass;
	}

	/**
	 * Sets the driver class.
	 *
	 * @param driverClass the new driver class
	 */
	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}

	/**
	 * Gets the jdbc url.
	 *
	 * @return the jdbc url
	 */
	public String getJdbcURL() {
		return jdbcURL;
	}

	/**
	 * Sets the jdbc url.
	 *
	 * @param jdbcURL the new jdbc url
	 */
	public void setJdbcURL(String jdbcURL) {
		this.jdbcURL = jdbcURL;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

    
    
}
