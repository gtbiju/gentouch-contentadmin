package com.cognizant.le.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

@WebServlet("/AgentUploadAction")
public class AgentUploadAction extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6833439222439684636L;
	private static Logger LOGGER = Logger.getLogger(LoginAction.class);
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("jsp/agentprofileupload.jsp").forward(request, response);
	}

}
