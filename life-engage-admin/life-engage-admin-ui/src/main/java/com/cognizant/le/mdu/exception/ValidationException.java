package com.cognizant.le.mdu.exception;

public class ValidationException extends Exception {

	private static final long serialVersionUID = 6045704508442464362L;
	
	public ValidationException(String message) {
		super(message);
	}

}
