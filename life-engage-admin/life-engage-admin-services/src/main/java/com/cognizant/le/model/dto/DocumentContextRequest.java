package com.cognizant.le.model.dto;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DocumentContextRequest {
	
	private List<Documents> contentFiles;
	private RequestInfo requestInfo;
	public List<Documents> getContentFiles() {
		return contentFiles;
	}
	public void setContentFiles(List<Documents> contentFiles) {
		this.contentFiles = contentFiles;
	}
	public RequestInfo getRequestInfo() {
		return requestInfo;
	}
	public void setRequestInfo(RequestInfo requestInfo) {
		this.requestInfo = requestInfo;
	}
}
