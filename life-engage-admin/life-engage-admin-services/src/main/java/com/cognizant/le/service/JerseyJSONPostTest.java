package com.cognizant.le.service;

import java.util.ArrayList;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.cognizant.le.model.dto.ContentFile;
import com.cognizant.le.model.dto.Documents;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class JerseyJSONPostTest {
	static Logger logger = Logger.getLogger(JerseyJSONPostTest.class);

	public static void main(String[] args) {

		try {

			Client client = Client.create();
			
			//WebResource webResource = client
					//.resource("http://localhost:8080/MLIDocumentWS/rest/documentService/getupdatedDocs");
			WebResource webResource = client
			.resource("http://localhost:8080/MLIDocumentWS/rest/documentService/getUpdatedDocumentsForContext");
			/*
			 * WebResource webResource = client .resource(
			 * "http://localhost:8080/MLIDocumentWS/rest/documentService/getupdatedDocs"
			 * );
			 */

			String inputtest1 = "[{\"fileName\":\"faq02.png\",\"lastUpdated\":\"01-Mar-2013\",\"publicURL\":\"\",\"tabletFullPath\":\"\",\"version\":\"1\"},{\"fileName\":\"faq03.png\",\"lastUpdated\":\"01-Mar-2013\",\"publicURL\":\"\",\"tabletFullPath\":\"\",\"version\":\"1\"},{\"fileName\":\"faq04.png\",\"lastUpdated\":\"01-Mar-2013\",\"publicURL\":\"\",\"tabletFullPath\":\"\",\"version\":\"1\"},{\"fileName\":\"faq05.png\",\"lastUpdated\":\"01-Mar-2013\",\"publicURL\":\"\",\"tabletFullPath\":\"\",\"version\":\"1\"}]";
			String contextRequest = "{\"contentFiles\" :[{\"fileName\":\"faq02.png\",\"lastUpdated\":\"01-Mar-2013\",\"publicURL\":\"\",\"tabletFullPath\":\"\",\"version\":\"1\"},{\"fileName\":\"faq03.png\",\"lastUpdated\":\"01-Mar-2013\",\"publicURL\":\"\",\"tabletFullPath\":\"\",\"version\":\"1\"},{\"fileName\":\"faq04.png\",\"lastUpdated\":\"01-Mar-2013\",\"publicURL\":\"\",\"tabletFullPath\":\"\",\"version\":\"1\"},{\"fileName\":\"faq05.png\",\"lastUpdated\":\"01-Mar-2013\",\"publicURL\":\"\",\"tabletFullPath\":\"\",\"version\":\"1\"}],\"requestInfo\" :{\"appContext\":\"msales\"}}";
			
			//ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).header("nwsAuthToken", "e03364fd-a8c6-7878-3d44-cff2a9d8e333")
				//	.post(ClientResponse.class, inputtest1);
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).header("nwsAuthToken", "e03364fd-a8c6-7878-3d44-cff2a9d8e333")
					.post(ClientResponse.class, contextRequest);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			System.out.println("Output from Server .... \n");
			String output = response.getEntity(String.class);
			System.out.println(output.toString());

		} catch (Exception e) {
			logger.error("Exception in JerseyJSONPostTest.main() in MLIDocumentWS",e);
		}

	}

}
