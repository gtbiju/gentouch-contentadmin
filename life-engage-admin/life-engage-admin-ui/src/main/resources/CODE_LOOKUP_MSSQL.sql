CREATE TABLE [~~~].[CODE_LOOKUP] (
  [id] bigint NOT NULL IDENTITY,
  [basicDataCompleteCode] int DEFAULT NULL,
  [contextId] INT DEFAULT NULL,
  [creationDateTime] DATE DEFAULT NULL,
  [transactionId] varchar(255) DEFAULT NULL,
  [typeName] varchar(255) DEFAULT NULL,
  [Code] varchar(255) DEFAULT NULL,
  [Description] varchar(255) DEFAULT NULL,
  [is_default] tinyint DEFAULT NULL,
  [codeType_id] bigint DEFAULT NULL FOREIGN KEY REFERENCES [~~~].[CODE_TYPE_LOOKUP] ([id]) ,
  PRIMARY KEY ([id])
); 