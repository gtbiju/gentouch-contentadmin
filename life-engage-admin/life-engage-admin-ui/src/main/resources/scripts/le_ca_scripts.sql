-- MySQL dump 10.13  Distrib 5.1.51, for Win32 (ia32)
--
-- Host: localhost    Database: le_contentadmin
-- ------------------------------------------------------
-- Server version	5.1.51-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `AGENT_APP_LOGIN`
--

LOCK TABLES `AGENT_APP_LOGIN` WRITE;
/*!40000 ALTER TABLE `AGENT_APP_LOGIN` DISABLE KEYS */;

INSERT INTO `AGENT_APP_LOGIN`(`userid`,`password`,`active`,`agentcode`) VALUES ('admin','1dce84d85ac49554d286ad6cb725d449','active',null);
INSERT INTO `AGENT_APP_LOGIN`(`userid`,`password`,`active`,`agentcode`) VALUES ('admin1','*4ACFE3202A5FF5CF467','active',null);
INSERT INTO `AGENT_APP_LOGIN`(`userid`,`password`,`active`,`agentcode`) VALUES ('admin2','*4ACFE3202A5FF5CF467898FC58AAB1D615029441','active',null);
/*!40000 ALTER TABLE `AGENT_APP_LOGIN` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Dumping data for table `APPLICATION_CONTEXTS`
--

LOCK TABLES `APPLICATION_CONTEXTS` WRITE;
/*!40000 ALTER TABLE `APPLICATION_CONTEXTS` DISABLE KEYS */;
INSERT INTO `APPLICATION_CONTEXTS`(`Id`,`Context`) VALUES (1,'common');
INSERT INTO `APPLICATION_CONTEXTS`(`Id`,`Context`) VALUES (2,'msales');
INSERT INTO `APPLICATION_CONTEXTS`(`Id`,`Context`) VALUES (3,'m-app');
INSERT INTO `APPLICATION_CONTEXTS`(`Id`,`Context`) VALUES (4,'lifeengage');

/*!40000 ALTER TABLE `APPLICATION_CONTEXTS` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Dumping data for table `CONTENT_TYPES`
--

LOCK TABLES `CONTENT_TYPES` WRITE;
/*!40000 ALTER TABLE `CONTENT_TYPES` DISABLE KEYS */;

insert into `CONTENT_TYPES`(`Id`,`Type`,`priority`,`extensions`,`optionality`,`previewable`) values (1,'PDF',3,'pdf',1,1);
insert into `CONTENT_TYPES`(`Id`,`Type`,`priority`,`extensions`,`optionality`,`previewable`) values (2,'Image',2,'jpg,jpeg,png',1,1);
insert into `CONTENT_TYPES`(`Id`,`Type`,`priority`,`extensions`,`optionality`,`previewable`) values (3,'Offline DB',1,'db',0,0);
insert into `CONTENT_TYPES`(`Id`,`Type`,`priority`,`extensions`,`optionality`,`previewable`) values (4,'Validation Rule',1,'js',0,0);
insert into `CONTENT_TYPES`(`Id`,`Type`,`priority`,`extensions`,`optionality`,`previewable`) values (5,'Illustration Rule',1,'js',0,0);
insert into `CONTENT_TYPES`(`Id`,`Type`,`priority`,`extensions`,`optionality`,`previewable`) values (6,'Video',4,'avi,wmv,mpg,mpeg,mkv,3gp,mp4',1,1);
insert into `CONTENT_TYPES`(`Id`,`Type`,`priority`,`extensions`,`optionality`,`previewable`) values (7,'Config Files',1,'properties,json',0,1);
insert into `CONTENT_TYPES`(`Id`,`Type`,`priority`,`extensions`,`optionality`,`previewable`) values (8,'HTML',1,'html,htm',1,1);
insert into `CONTENT_TYPES`(`Id`,`Type`,`priority`,`extensions`,`optionality`,`previewable`) values (9,'dummy',5,'xls',1,0);

/*!40000 ALTER TABLE `CONTENT_TYPES` ENABLE KEYS */;
UNLOCK TABLES;


INSERT INTO CONTENT_LANGUAGES (id, name) VALUES('1', 'English'), ('2', 'Thai'), ('3', 'Malayalam'), ('4', 'Hindi'),('5', 'Tamil'), ('6', 'Telugu'), ('7', 'Kannada'), ('8', 'Marathi');