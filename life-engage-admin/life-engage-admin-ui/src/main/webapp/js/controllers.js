'use strict';

/* Controllers */

angular.module('myApp.controllers', ['ui.state']).
  controller('TabsDemoCtrl', ['$scope',function ($scope) {
      //$scope.name = "Hari";
      $scope.tabs = [
    { title: "Dynamic Title 1", content: "Dynamic content 1" },
    { title: "Dynamic Title 2", content: "Dynamic content 2", disabled: true }
   ];
      $scope.navType = 'pills';
	   // Datepicker directive
       $scope.datepicker = {date: new Date("2012-09-01T00:00:00.000Z")};

  } ])
  .controller('AccordionDemoCtrl', ['$scope',function ($scope) {
      /*accordion content opens one at a time if true*/
	  $scope.oneAtATime = true;
	  $scope.radioState = true;
      $scope.groups = 
	   [	
	     {title: "Insured Contact Details",url:'partials/dynamic-accordion-body1.html'},
	     {title: "Insured Occupation Details",url: "Dynamic Group Body - 2"}
	   ];     
      $scope.group = $scope.groups[0];
	  
	  $scope.enablePAN = function(){
		$scope.radioState = false;
	  };
	  $scope.acc1open = false;
  } ])
  
  .controller('DateCtrl', function($scope,$window,$location) {

  // Datepicker directive
  $scope.datepicker = {date: new Date("2012-09-01T00:00:00.000Z")};
  
});
;
