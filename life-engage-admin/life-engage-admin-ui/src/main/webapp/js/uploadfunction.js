var resultJson;
$(function () {
    $('#fileupload').fileupload({
		
        dataType: 'json',
 
        done: function (e, data) {
			var error =false;
            $("#sidebar").html('');
			$("#detail_table").html('');
            $.each(data.result, function (index, table) {
			resultJson = data.result;
            	if(table!=null && table.responseStatus!='ERROR'){
				var sideBarContent = "";
				if(index==0){
				sideBarContent +='<a href="#" id="'+table.tableName+'" class="list-group-item active" onClick="linkHandler(this)"> <span class="glyphicon glyphicon-file"></span>';
				sideBarContent += table.tableName;
				sideBarContent += '<span class="badge">'+table.dataCount+'</span></a>';
				$("#sidebar").append(sideBarContent);

					var headerArray = [];
					// Select the first Object in the Array
					// and iterate over it
					
					$.each(table.masterDbEntries[0] , function(key, value){
					   headerArray.push(key)
					});
					
					console.log(headerArray);
					var tableDetails = '<thead><tr style="background-color: #f5f5f5">'; 
					$.each( headerArray, function( key, value ) {
					tableDetails += '<th style="text-transform:uppercase;">'+value+'</th>';
					});
					tableDetails += '</tr></thead><tbody>';

					$.each(table.masterDbEntries , function(key, value){
					tableDetails += '<tr>';
					
					   $.each(value, function(key,value){
					   console.log(value);
					   tableDetails +='<td>'+value+'</td>'; 
					   });
					   tableDetails +='</tr>'; 
					});
					tableDetails +='</tbody>'; 
					
				   $("#detail_table").append(tableDetails);
				}
				else{
				
				sideBarContent += '<a href="#" id="'+table.tableName+'" class="list-group-item" onClick="linkHandler(this)"> <span class="glyphicon glyphicon-file"></span>';
				sideBarContent += table.tableName;
				sideBarContent += '<span class="badge">'+table.dataCount+'</span></a>';
                $("#sidebar").append(sideBarContent);
                    //end $("#uploaded-files").append()
				}
				
				$(this).find('.progress')
                       .attr('aria-valuenow', '0')
                       .children().first().css('width', '0%');
                $(this).find('.progress-extended').html('&nbsp;');	
			}else if(null!=table && table.responseStatus == 'ERROR'){
				error =true;
				$("#error-body").html(table.message);
				$("#messageModal").modal('show');
			}			
            }); 
			if(!error){
				$("#myModal").modal('show');
			}
			$('#presentation-table tr').remove();
        },
 
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        },
		stop: function (e, data) {
			 
		},
 
        dropZone: $('#dropzone')
    });
	
});

	function linkHandler(obj){
	var headerArray = [];
	var tableDetails="";
	
	if($("#sidebar").find('a').hasClass("active")){
		$("#sidebar").find('a').removeClass("active");
		$("#"+obj.id).addClass("active");
	}
	
	$("#detail_table").dataTable().fnDestroy();
		$.each(resultJson, function (index, table) 
			{ 
				if(table!=null && table.tableName==obj.id)
					{
					// Select the first Object in the Array
					// and iterate over it
						$.each(table.masterDbEntries[0] , function(key, value){
						headerArray.push(key)
						});
						tableDetails = '<thead><tr style="background-color: #f5f5f5">'; 
						$.each( headerArray, function( key, value ) {
							tableDetails += '<th style="text-transform:uppercase;">'+value+'</th>';
						});
						tableDetails += '</tr></thead><tbody>';


						$.each(table.masterDbEntries , function(key, value){
							tableDetails += '<tr>';
					
					    $.each(value, function(key,value){
					    tableDetails +='<td>'+value+'</td>'; 
					    });
					   tableDetails +='</tr>'; 
					});
					tableDetails +='</tbody>'; 
					
					}
			});
				   $("#detail_table").html('');
				   $("#detail_table").append(tableDetails);
				   
					var oTable= $('#detail_table').dataTable({
						"bSort": false,       // Disable sorting
						"iDisplayLength": 6   //records per page
					});	
	}
	
	
