package com.cognizant.le.model.dto;

// TODO: Auto-generated Javadoc
/**
 * The Class CodeLocaleLookup.
 */
public class CodeLocaleLookup extends MasterDBEntry {
	
	/** The code id. */
	private int codeId;
	
	/** The language. */
	private String language;
	
	/** The country. */
	private String country;
	
	/** The value. */
	private String value;

	/**
	 * Gets the code id.
	 *
	 * @return the code id
	 */
	public int getCodeId() {
		return codeId;
	}

	/**
	 * Sets the code id.
	 *
	 * @param codeId the new code id
	 */
	public void setCodeId(int codeId) {
		this.codeId = codeId;
	}

	/**
	 * Gets the language.
	 *
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the language.
	 *
	 * @param language the new language
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
