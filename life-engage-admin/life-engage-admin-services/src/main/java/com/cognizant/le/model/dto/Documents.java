package com.cognizant.le.model.dto;


import org.codehaus.jackson.annotate.JsonIgnoreProperties;


/**
 * The Class Documents.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Documents {

	/** The file name. */
	private String fileName;
	
	/** The tablet full path. */
	private String tabletFullPath;
	
	/** The public url. */
	private String publicURL;
	
	/** The last updated. */
	private String lastUpdated;
	
	/** The version. */
	private String version;
	
	/** The type. */
	private String type;
	
	/** The is mandatory. */
	private String isMandatory;
	
	/** The language. */
	private String language;
	
	/** The file size. */
	private Long fileSize;
	
	/** The description. */
	private String description;

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the tablet full path.
	 *
	 * @return the tablet full path
	 */
	public String getTabletFullPath() {
		return tabletFullPath;
	}

	/**
	 * Sets the tablet full path.
	 *
	 * @param tabletFullPath the new tablet full path
	 */
	public void setTabletFullPath(String tabletFullPath) {
		this.tabletFullPath = tabletFullPath;
	}

	/**
	 * Gets the public url.
	 *
	 * @return the public url
	 */
	public String getPublicURL() {
		return publicURL;
	}

	/**
	 * Sets the public url.
	 *
	 * @param publicURL the new public url
	 */
	public void setPublicURL(String publicURL) {
		this.publicURL = publicURL;
	}

	/**
	 * Gets the last updated.
	 *
	 * @return the last updated
	 */
	public String getLastUpdated() {
		return lastUpdated;
	}

	/**
	 * Sets the last updated.
	 *
	 * @param lastUpdated the new last updated
	 */
	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version the new version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the checks if is mandatory.
	 *
	 * @return the checks if is mandatory
	 */
	public String getIsMandatory() {
		return isMandatory;
	}

	/**
	 * Sets the checks if is mandatory.
	 *
	 * @param isMandatory the new checks if is mandatory
	 */
	public void setIsMandatory(String isMandatory) {
		this.isMandatory = isMandatory;
	}

	/**
	 * Gets the language.
	 *
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the language.
	 *
	 * @param language the new language
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * Gets the file size.
	 *
	 * @return the file size
	 */
	public Long getFileSize() {
		return fileSize;
	}

	/**
	 * Sets the file size.
	 *
	 * @param fileSize the new file size
	 */
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
